package org.stellar.sdk;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;


public class DeviceInfo {

    @SerializedName("Model")
    private String model;
    @SerializedName("Manufacturer")
    private String manufacturer;
    @SerializedName("Mac")
    private String macAddress;
    @SerializedName("OsVersion")
    private int sdkVersion;
    @SerializedName("Locale")
    private String locale;

    public String getModel() {
        return model;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public int getSdkVersion() {
        return sdkVersion;
    }

    public String getLocale() {
        return locale;
    }

    public DeviceInfo(String model, String manufacturer, String macAddress, int sdkVersion, String locale) {
        this.model = model;
        this.manufacturer = manufacturer;
        this.macAddress = macAddress;
        this.sdkVersion = sdkVersion;
        this.locale = locale;
    }

    public String toJson(){
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    public static DeviceInfo fromJson(String json){
        Gson gson = new Gson();
        return gson.fromJson(json, DeviceInfo.class);
    }
}

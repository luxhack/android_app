package org.stellar.sdk;

import android.os.Build;

import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class DeviceInfoManager {
    public static DeviceInfo obtainDeviceInfo(){
        String macAddress = obtainWifiMacAddress();

        return new DeviceInfo(Build.MODEL,
                Build.MANUFACTURER,
                macAddress,
                Build.VERSION.SDK_INT,
                Locale.getDefault().toString());
    }

    private static String obtainWifiMacAddress() {
        try {
            final String interfaceName = "wlan0";
            List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
            for (NetworkInterface intf : interfaces) {
                if (!intf.getName().equalsIgnoreCase(interfaceName)){
                    continue;
                }

                byte[] mac = intf.getHardwareAddress();
                if (mac==null){
                    return "";
                }

                StringBuilder buf = new StringBuilder();
                for (byte aMac : mac) {
                    buf.append(String.format("%02X:", aMac));
                }
                if (buf.length()>0) {
                    buf.deleteCharAt(buf.length() - 1);
                }
                return buf.toString();
            }
        } catch (Exception ex) { } // for now eat exceptions
        return "";
    }
}

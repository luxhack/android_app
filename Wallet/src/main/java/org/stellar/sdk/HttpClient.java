package org.stellar.sdk;


import android.util.Log;

import com.google.gson.reflect.TypeToken;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.AbstractHttpEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.stellar.sdk.federation.NotFoundException;
import org.stellar.sdk.federation.ServerErrorException;
import org.stellar.sdk.requests.ResponseHandler;
import org.stellar.sdk.responses.GsonSingleton;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.util.logging.Logger;

public class HttpClient {

    private static org.apache.http.client.HttpClient httpClient;

    static {
        final int timeout = 60 * 1000;
        RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(timeout)
                .setSocketTimeout(timeout).setConnectionRequestTimeout(timeout).build();
        httpClient = HttpClientBuilder.create().setDefaultRequestConfig(requestConfig).build();
    }

    public static <T> T executeJsonPostRequest(final URI uri, final String serializedJson,
                                               TypeToken<T> type) {
        StringEntity entity;
        try {
            entity = new StringEntity(serializedJson);
            entity.setContentType("application/json");
        } catch (UnsupportedEncodingException e) {
            throw new ServerErrorException(e);
        }
        return executePostRequest(uri, entity, type, false);
    }

    public static <T> T executeJsonPostRequest(final URI uri, final Object bodyData,
                                               TypeToken<T> type) {
        final String serializedJson = GsonSingleton.getInstance().toJson(bodyData);
        Log.d("executeJsonPostRequest", "serializedJson" + serializedJson);
        return executeJsonPostRequest(uri, serializedJson, type);
    }

    public static <T> T executePostRequest(final URI uri, final AbstractHttpEntity entity, TypeToken<T> type, boolean ignoreServerErrors) {
        HttpPost request = new HttpPost(uri);
        Log.d("executePostRequest", "executePostRequest: " + uri.getPath());
        request.setEntity(entity);
        return executeRequest(request, type, ignoreServerErrors);
    }

    public static <T> T executeGetRequest(final URI uri, TypeToken<T> type) {
        final HttpGet request = new HttpGet(uri);
        return executeRequest(request, type, false);
    }

    public static String executeJsonGetRequest(final URI uri) {
        Log.e("!!!!!", uri.getPath());
        final HttpGet request = new HttpGet(uri);
        return executeRequest(request);
    }

    public static <T> T executeRequest(final HttpRequestBase request,
                                       TypeToken<T> type, final boolean ignoreServerErrors) {
        try {
            setDeviceInfoHeaders(request);

            final ResponseHandler<T> responseHandler = new ResponseHandler<T>(type, ignoreServerErrors);
            final HttpResponse response = httpClient.execute(request);

            Log.d("executeRequest", "request: " + request);
            Log.d("executeRequest", "response: " + response.getEntity().getContent());

            return responseHandler.handleResponse(response);

        } catch (HttpResponseException e) {
            if (e.getStatusCode() == 404) {
                throw new NotFoundException(e.getMessage());
            } else {
                throw new ServerErrorException();
            }
        } catch (IOException e) {
            throw new ConnectionErrorException();
        }
    }

    public static String executeRequest(final HttpRequestBase request) {
        try {
            setDeviceInfoHeaders(request);

            final HttpResponse response = httpClient.execute(request);

            Log.d("executeRequest", "request: " + request);
            Log.d("executeRequest", "response: " + response.getEntity().getContent());

            String jsonString = IOUtils.toString(response.getEntity().getContent(), "UTF-8");

            Log.d("executeRequest", "jsonString: " + jsonString);

            return jsonString;

        } catch (HttpResponseException e) {
            if (e.getStatusCode() == 404) {
                throw new NotFoundException(e.getMessage());
            } else {
                throw new ServerErrorException();
            }
        } catch (IOException e) {
            throw new ConnectionErrorException();
        }
    }

    private static void setDeviceInfoHeaders(final HttpRequestBase request){
        DeviceInfo info = DeviceInfoManager.obtainDeviceInfo();

        request.setHeader("Device-Locale", info.getLocale());
        request.setHeader("Device-Manufacturer", info.getManufacturer());
        request.setHeader("Device-Model", info.getModel());
        request.setHeader("Device-SDK-Version", String.valueOf(info.getSdkVersion()));
        request.setHeader("Device-Mac-Address", info.getMacAddress());
    }

    public static void setHttpClient(org.apache.http.client.HttpClient httpClient) {
        HttpClient.httpClient = httpClient;
    }
}

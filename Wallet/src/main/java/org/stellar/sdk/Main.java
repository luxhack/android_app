package org.stellar.sdk;

import org.stellar.sdk.federation.Federation;
import org.stellar.sdk.federation.FederationResponse;
import org.stellar.sdk.responses.*;

import java.io.IOException;
import java.util.List;

public class Main {


    private static final String ROOT_SEED = "SDHOAMBNLGCE2MV5ZKIVZAQD3VCLGP53P3OBSBI6UN5L5XZI5TKHFQL4";

    /*
    accountId: GANQ67PB5WFXR3UN32YVERSFLNCHQMYRNTQKPKV37X2HKBRXTJJR32XP
seed: SCSBNAWJUCOHATF6AJL2B6OOMHDA7ITWST4OGIPPPMTY6QRMQ5ZCD5LU
     */
    public static void main(String[] args) throws Exception{

        balances();


        //final String str = destAccount.toString();
        //System.out.println(str);
        /*KeyPair source = KeyPair.fromSecretSeed("SB5MGOMXKRHDC5OSM26QEJBZWIWNWFSQRQARMPZG4XFSUPQQIWUXTVO6");
        KeyPair destination = KeyPair.fromAccountId("GDW6AUTBXTOC7FIKUO5BOO3OGLK4SF7ZPOBLMQHMZDI45J2Z6VXRB5NR");
        Account account = new Account(source, 0L);
        Transaction transaction = new Transaction.Builder(account)
                .addOperation(new CreateAccountOperation.Builder(destination, "0").build())
                .build();

        transaction.sign(source);
        Server s = new Server("http://cobranewcore.cloudapp.net:9003");
        for (int i = 0; i < 10; i++)
        {
            SubmitTransactionResponse resp = s.submitTransaction(transaction);
            System.out.println(resp.getTransactionResult());
        }*/
    }

    private static final String HORIZON_URI = "http://newcobradev.cloudapp.net:8000";
    private static final String API_URI = "http://newcobradev.cloudapp.net:3001";
    private static final String DESTINATION_NAME = "dmitriy";
    private static final String ASSET_CODE = "EUR";
    private static final String SENDER_SEED = "SCSBNAWJUCOHATF6AJL2B6OOMHDA7ITWST4OGIPPPMTY6QRMQ5ZCD5LU";
    private static final Server SERVER = new Server(HORIZON_URI, API_URI);

    private static boolean createTrustLine(final KeyPair from, final Asset[] assets) throws IOException {
        final AccountResponse account = SERVER.accounts().account(from);
        final Transaction.Builder tr = new Transaction.Builder(account);
        for (final Asset asset : assets) {
            tr.addOperation(new ChangeTrustOperation.Builder(asset, Integer.MAX_VALUE + "").build());
        }
        final Transaction tx = tr.build();
        tx.sign(from);
        return SERVER.submitTransaction(tx).isSuccess();
    }

    private static boolean createAccount(final KeyPair creator, final KeyPair account) throws IOException {
        final AccountResponse creatorAccount = SERVER.accounts().account(creator);
        final Transaction tx = new Transaction.Builder(creatorAccount).addOperation(
                new CreateAccountOperation.Builder(account, "0").build()
        ).build();
        tx.sign(creator);
        final SubmitTransactionResponse txResp = SERVER.submitTransaction(tx);
        return txResp.isSuccess();
    }

    private static boolean createOffer(final KeyPair creator, final Asset sell, final Asset buy, String amount,
                                       String price, boolean isBuying) throws IOException {
        final AccountResponse creatorAccount = SERVER.accounts().account(creator);
        final Operation op;
        if (isBuying)
            op = ManageOfferOperation.Builder.buy(sell, buy, amount, price).build();
        else
            op = ManageOfferOperation.Builder.sell(sell, buy, amount, price).build();
        final Transaction tx = new Transaction.Builder(creatorAccount).addOperation(op).build();
        tx.sign(creator);
        final SubmitTransactionResponse txResp = SERVER.submitTransaction(tx);
        return txResp.isSuccess();
    }

    private static boolean createAccount(final KeyPair creator, final KeyPair account, final Asset[] assets) throws IOException {
        return createAccount(creator, account) && createTrustLine(account, assets);
    }

    private static class Pay {
        final Asset asset;
        final String amount;

        public Pay(Asset asset, String amount) {
            this.asset = asset;
            this.amount = amount;
        }
    }

    private static boolean sendMoney(final KeyPair from, final KeyPair to, Pay[] pays) throws IOException {
        final AccountResponse fromAccount = SERVER.accounts().account(from);
        final Transaction.Builder txBuilder = new Transaction.Builder(fromAccount);
        for (Pay pay : pays) {
            txBuilder.addOperation(new PaymentOperation.Builder(to, pay.asset, pay.amount).build());
        }
        final Transaction tx = txBuilder.build();
        tx.sign(from);
        return SERVER.submitTransaction(tx).isSuccess();
    }

    private static PathResponse selectBest(List<PathResponse> paths) {
        return paths.get(0);
    }

    public static void balances() throws IOException {
        final KeyPair ISSUER_ACCOUNT_ID = KeyPair.fromSecretSeed(ROOT_SEED);
        System.out.println(ISSUER_ACCOUNT_ID.getAccountId());
        final long INVOICE_ID = 2;

        Network.usePublicNetwork(); // setup network type. Done once per app run.
        /*final KeyPair root = KeyPair.fromSecretSeed(ROOT_SEED);
        final Asset usd = Asset.createNonNativeAsset("USD", root);
        final Asset eur = Asset.createNonNativeAsset("EUR", root);
        final KeyPair usdAccount = KeyPair.fromSecretSeed("SCYU43TRYXUHULMQ4W3XF23KQCAKXMC2XNI6THBLG3B4HO2R3VVVVPQZ");
        System.out.println("USD account: " + usdAccount.getAccountId());
        final KeyPair eurAccount = KeyPair.fromSecretSeed("SCLZI2XEG3QJBI7TDXOXP2XDYQ2GRPNMCWDN45RDZPWSPKOS4ZLBF54K");
        final List<PaymentOperationResponse> paymentOperations = SERVER.payments().forAccount(usdAccount);
        for (PaymentOperationResponse p : paymentOperations) {
            System.out.println("Spend: " + p.getAmount() + " asset: " + p.getAsset());

        }*/


        final UserInfo userInfo = SERVER.getUserInfo(20);
        if (userInfo.isSuccess() && userInfo.getUser().getState() == UserState.APPROVED)
            System.out.println("User is approved");
        final KeyPair ROOT = KeyPair.fromSecretSeed(ROOT_SEED);
        final Asset USD = Asset.createNonNativeAsset("USD", ROOT);
        final Asset EUR = Asset.createNonNativeAsset("EUR", ROOT);
        final Asset[] assets = new Asset[] {USD, EUR};
        final KeyPair a = KeyPair.random();
        final KeyPair b = KeyPair.random();
        final KeyPair c = KeyPair.random();
        //createAccount(ROOT, a);
        //final KeyPair b = KeyPair.random();
        //Server.coreURI = URI.create("http://newcobradev.cloudapp.net:8080");
        createAccount(ROOT, a);
        createTrustLine(a, assets);
        sendMoney(ROOT, a, new Pay[] {new Pay(USD, "1000")});
        createAccount(ROOT, b);
        createTrustLine(b, assets);
        sendMoney(ROOT, b, new Pay[] {new Pay(EUR, "1000")});
        createAccount(ROOT, c);
        createTrustLine(c, assets);
        sendMoney(ROOT, c, new Pay[] {new Pay(EUR, "1000")});
        createOffer(a, USD, EUR, "500", "1.5", false);
        createOffer(b, USD, EUR, "100", "1.5", true);
        createOffer(c, USD, EUR, "90", "1.5", true);
        //manageFlags(a, a, AccountFlags.BANK_ADMIN.getValue(), 0);//fail
        //createAccount(ROOT, KeyPair.random());
        //manageFlags(a, b, AccountFlags.BANK_ADMIN.getValue(), 0);//fail
        //manageFlags(a, ROOT, 0, AccountFlags.CREATE_BANK_ADMIN.getValue());//fail

        //manageFlags(ROOT, a, AccountFlags.BANK_ADMIN.getValue(), 0);//ok
        //manageFlags(a, a, AccountFlags.BANK_ADMIN.getValue(), 0);//fail
        //manageFlags(a, b, AccountFlags.BANK_ADMIN.getValue(), 0);//fail
        //manageFlags(a, ROOT, 0, AccountFlags.CREATE_BANK_ADMIN.getValue());//fail

        //manageFlags(ROOT, a, AccountFlags.CREATE_BANK_ADMIN.getValue(), 0);//ok
        //manageFlags(ROOT, a, 0, AccountFlags.CREATE_BANK_ADMIN.getValue());//ok

        FederationResponse f = Federation.resolve("test");
        System.out.println(ROOT.getAccountId());
        System.out.println(f.getAccountId());
        sendMoney(ROOT, KeyPair.fromAccountId(f.getAccountId()), new Pay[]{new Pay(USD, "100")});
        final KeyPair buyer = KeyPair.fromAccountId("GA2PAU6P4WW2Z2AUCBD3RPVGDSGTYIZODUBRWAE43WYJNMO43Z72JJI5");//KeyPair.random();
        final KeyPair seller = KeyPair.random();
        System.out.println("buyer: " + buyer.getAccountId() + "\nseller: " + seller.getAccountId());
        sendMoney(ROOT, buyer, new Pay[] {new Pay(USD, "10000"), new Pay(EUR, "10000")});
        //createOffer(root, eur, usd, "50000000", "1.5", true);
        //createOffer(root, eur, usd, "50000000", "1.6", false);
        createAccount(ROOT, seller, assets);
        sendMoney(ROOT, seller, new Pay[] {new Pay(USD, "100"), new Pay(EUR, "100")});
        /*final KeyPair dest = KeyPair.random();
        final Transaction tx = new Transaction.Builder(rootAccount).addOperation(
                new CreateAccountOperation.Builder(dest, "0").build()).build();
        tx.sign(root);
        final TransactionResponse createResp = SERVER.submitTransaction(tx);
        if (createResp.isSuccess()) {
            final AccountResponse destAccount = SERVER.accounts().account(dest);
            final Transaction trustTx = new Transaction.Builder(destAccount)
                    .addOperation(new ChangeTrustOperation.Builder(Asset.createNonNativeAsset("USD", root), "1000").build()).build();
            trustTx.sign(dest);
            SERVER.submitTransaction(trustTx);
        }*/
        /*final AccountResponse rootAccount = SERVER.accounts().account(root);
        final KeyPair newAccount = KeyPair.random();
        final Transaction tx = new Transaction.Builder(rootAccount).addOperation(
                new CreateAccountOperation.Builder(newAccount, "0").build()).build();
        tx.sign(root);
        if (SERVER.submitTransaction(tx).isSuccess()) {
            if (createTrustLine(newAccount, new Asset[] {Asset.createNonNativeAsset("USD", ISSUER_ACCOUNT_ID),
                    Asset.createNonNativeAsset("EUR", ISSUER_ACCOUNT_ID)}))
                System.out.println("Submitted");
        }
        System.out.println("accountId: " + newAccount.getAccountId() + "\nseed: " + newAccount.getSecretSeed());*/
        /*final Transaction tr = new Transaction.Builder(rootAccount).addOperation(
                new CreateAccountOperation.Builder(newAccount, "0").build()
        ).build();
        tr.sign(root);
        final TransactionResponse response = SERVER.submitTransaction(tr);*/
        //if (response.isSuccess()) {
        /*    final boolean trustLineRes = createTrustLine(newAccount, new Asset[] {Asset.createNonNativeAsset("USD", ISSUER_ACCOUNT_ID),
                Asset.createNonNativeAsset("EUR", ISSUER_ACCOUNT_ID)});
            System.out.println("trustline creation: " + trustLineRes);*/
        //}
        /*final Transaction payment = new Transaction.Builder(rootAccount).addOperation(
                new PaymentOperation.Builder(newAccount, Asset.createNonNativeAsset("EUR", ISSUER_ACCOUNT_ID), "3").build())
                .addOperation(new PaymentOperation.Builder(KeyPair.fromAccountId("GCKQULOL4E7JAXQXEFN66ICCTLUAU75S52FKUF4JW6LT53IFFIKS7Z5D")
                        , Asset.createNonNativeAsset("EUR", ISSUER_ACCOUNT_ID), "3").build()).build();
        payment.sign(root);
        final TransactionResponse paymentResponse = SERVER.submitTransaction(payment);
        System.out.println(paymentResponse);*/
        /*final String login = "arma@cobra.com";
        final String password = "register";
        final KeyPair keyPair = KeyPair.random();
        final boolean register = SERVER.signUp("Dmitriy", "Anderson", "Dmitiry@gmail.com", "+3809578653902", login,
                password, keyPair);
        if (register) {
            //admin must approve account
            System.out.println("Check approval");
            final KeyPair storedKeys = SERVER.wallet().getWallet(login, password);
            System.out.println(storedKeys.getSecretSeed().equals(keyPair.getSecretSeed()));
        }
        System.out.println(register);*/
        /*final boolean isSaved = SERVER.wallet().createWallet(login, password, KeyPair.fromSecretSeed(SENDER_SEED));
        if (isSaved) {
            final KeyPair retrivedKey = SERVER.wallet().getWallet(login, password);
            System.out.println(retrivedKey.getSecretSeed());
        }
        /*final List<Invoice> invoices = SERVER.invoices().forAccount(senderKeyPair);
        for (Invoice invoice : invoices) {
            final AccountResponse sender = SERVER.accounts().account(senderKeyPair);
            final Transaction tr = new Transaction.Builder(sender)
                    .addOperation(new PaymentOperation.Builder(
                            invoice.getSeller(),
                            invoice.getAsset(),
                            invoice.getAmount()).build()).build();
            tr.sign(senderKeyPair);
            final TransactionResponse trResponse = SERVER.submitTransaction(tr);
            if (trResponse.isSuccess()) {
                final Invoice paymentResponse = SERVER.invoices().submitPayment(invoice.getId(),
                        trResponse.getHash());
                if (paymentResponse != null) {
                    //success
                }
            }
        }*/
    }

   /* private static void paymentExample(final String fromSeed, final String toAccountId) throws IOException {
        //send from "from" to "to"
        final KeyPair fromKeys = KeyPair.fromSecretSeed(fromSeed);
        //request account data to get sequence
        final AccountResponse from = SERVER.accounts().account(fromKeys);
        final Transaction tx = new Transaction.Builder(from).addOperation(new PaymentOperation.Builder(
                KeyPair.fromAccountId(toAccountId),
                Asset.createNonNativeAsset("USD", fromKeys),
                "100"
        ));
    }*/
}

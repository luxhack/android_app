package org.stellar.sdk;

import com.google.gson.reflect.TypeToken;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.stellar.sdk.requests.*;
import org.stellar.sdk.responses.UserInfo;
import org.stellar.sdk.responses.SubmitTransactionResponse;
import org.stellar.sdk.responses.WalletCreateResponse;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

/**
 * Main class used to connect to Horizon server.
 */
public class Server {
    private URI horizonURI;
    private URI apiURI;

    /**
     * Creates connector to horizon and core servers;
     * @param horizonURI - uri to horizon ("http://cobranewcore.cloudapp.net:8000")
     */
    public Server(final String horizonURI, final String apiURI) {
        try {
            this.horizonURI = new URI(horizonURI);
            this.apiURI = new URI(apiURI);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    public Server(final String horizonURI) {
        this(horizonURI, horizonURI);
    }

    /**
     * Returns {@link AccountsRequestBuilder} instance.
     */
    public AccountsRequestBuilder accounts() {
        return new AccountsRequestBuilder(horizonURI);
    }

    /**
     * Returns {@link EffectsRequestBuilder} instance.
     */
    public EffectsRequestBuilder effects() {
        return new EffectsRequestBuilder(horizonURI);
    }


    /**
     * Returns {@link OffersRequestBuilder} instance.
     */
    public OffersRequestBuilder offers() {
        return new OffersRequestBuilder(horizonURI);
    }

    public MatchesRequestBuilder matches() { return new MatchesRequestBuilder(horizonURI);}

    /**
     * Returns {@link OperationsRequestBuilder} instance.
     */
    public OperationsRequestBuilder operations() {
        return new OperationsRequestBuilder(horizonURI);
    }

    /**
     * Returns {@link OrderBookRequestBuilder} instance.
     */
    public OrderBookRequestBuilder orderBook() {
        return new OrderBookRequestBuilder(horizonURI);
    }

    public WalletRequestBuilder wallet() {
        return new WalletRequestBuilder(this.apiURI);
    }

    /**
     * Returns {@link PathsRequestBuilder} instance.
     */
    public PathsRequestBuilder paths() {
        return new PathsRequestBuilder(horizonURI);
    }

    /**
     * Returns {@link PaymentsRequestBuilder} instance.
     */
    public PaymentsRequestBuilder payments() {
        return new PaymentsRequestBuilder(horizonURI);
    }

    /**
     * Returns {@link TransactionsRequestBuilder} instance.
     */
    public TransactionsRequestBuilder transactions() {
        return new TransactionsRequestBuilder(horizonURI);
    }

    public UserInfo signUp(String firstName, String lastName, String email, String phoneNumber,
                           String login, String password, KeyPair account) {
        return new SigUpRequestBuilder(this.apiURI).signUp(firstName, lastName, email, phoneNumber,
                login, password, account);}

    public WalletCreateResponse createWallet(String login, String password, KeyPair account){
        return new WalletCreateRequestBuilder(this.apiURI).createWallet(login, password, account);
    }

    public UserInfo getUserInfo(final int id) {
        return new UserInfoRequestBuilder(this.apiURI).getInfo(id);
    }

    /**
     * Returns {@link TransactionRequestBuilder} instance.
     */
    public TransactionRequestBuilder transaction() {
        return new TransactionRequestBuilder(horizonURI);
    }

    public SubmitTransactionResponse submitTransaction(final Transaction transaction) throws UnsupportedEncodingException {
        URI transactionsURI;
        try {
            transactionsURI = new URIBuilder(this.horizonURI).setPath("/transactions").build();
        } catch (URISyntaxException e) {
            throw new AssertionError(e);
        }

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("tx", transaction.toEnvelopeXdrBase64()));
        return HttpClient.executePostRequest(transactionsURI,
                new UrlEncodedFormEntity(params),
                new TypeToken<SubmitTransactionResponse>(){}, true);
    }


}

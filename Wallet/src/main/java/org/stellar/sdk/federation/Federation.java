package org.stellar.sdk.federation;

import com.google.common.net.InternetDomainName;
import org.stellar.sdk.ConnectionErrorException;
import org.stellar.sdk.KeyPair;

/**
 * Helper class for resolving Stellar addresses.
 *
 * @see <a href="https://www.stellar.org/developers/learn/concepts/federation.html" target="_blank">Federation docs</a>
 */
public class Federation {

    private Federation() {
  }

  /**
   * This method is a helper method for handling user inputs that contain `destination` value.
   * It accepts two types of values:
   * <ul>
   * <li>For Stellar address (ex. <code>bob@stellar.org`</code>) it splits Stellar address and then tries to find information about
   * federation server in db file for a given domain.</li>
   * <li>For Stellar address (ex. <code>bob</code>) it tries to find information about
   * federation server in db file for a <code>Config.DEFAULT_DOMAIN</code> domain.</li>
   * <li>For account ID (ex. <code>GB5XVAABEQMY63WTHDQ5RXADGYF345VWMNPTN2GFUDZT57D57ZQTJ7PS</code>) it tries to find info
   * in <code>Config.DEFAULT_DOMAIN</code> domain. </li>
   * </ul>
   * @param value Stellar address or account id
   * @throws MalformedAddressException
   * @throws ConnectionErrorException
   * @throws NoFederationServerException
   * @throws FederationServerInvalidException
   * @throws NotFoundException
   * @throws ServerErrorException
   */
  public static FederationResponse resolve(String value) {
      FederationRequest request = FederationRequest.getFromValue(value);
      FederationServer server = FederationServer.createForDomain(InternetDomainName.from(request.domain));
      if (KeyPair.tryFromAccountId(request.name) != null)
          return server.resolveUsername(request);
      return server.resolveAddress(request);
  }
}

package org.stellar.sdk.federation;

import com.google.common.net.InternetDomainName;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

class FederationAddressProvider {

    private static Map<InternetDomainName, String> federations;

    static {
        federations = new HashMap<InternetDomainName, String>();
        federations.put(InternetDomainName.from("cobraapi.azurewebsites.net"), "http://cobrastaging.cloudapp.net:3001");
        federations.put(InternetDomainName.from("cobra.com"), "http://cobrastaging.cloudapp.net:3001");
    }

    public static String getURI(InternetDomainName domain) {
        return federations.get(domain);
    }
}

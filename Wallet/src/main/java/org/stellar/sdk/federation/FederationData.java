package org.stellar.sdk.federation;

import com.google.gson.annotations.SerializedName;

class FederationData {

    @SerializedName("accountId")
    private String accountId;
    @SerializedName("username")
    private String username;
    @SerializedName("domain")
    private String domain;
    @SerializedName("name")
    private String name;
    @SerializedName("firstName")
    private String firstName;
    @SerializedName("lastName")
    private String lastName;
    @SerializedName("approved")
    private boolean approved;

    public FederationData() {}

    public String getAccountId() {
        return accountId;
    }

    public String getUsername() {
        return username;
    }

    public String getDomain() {
        return domain;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }

    public String getName() {
        return name;
    }

    public FederationData setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public String toString() {
        return "FederationData{" +
                "accountId='" + accountId + '\'' +
                ", username='" + username + '\'' +
                ", domain='" + domain + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", approved=" + approved +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FederationData)) return false;

        FederationData that = (FederationData) o;

        if (getAccountId() != null ? !getAccountId().equals(that.getAccountId()) : that.getAccountId() != null) return false;
        if (getUsername() != null ? !getUsername().equals(that.getUsername()) : that.getUsername() != null)
            return false;
        return !(getDomain() != null ? !getDomain().equals(that.getDomain()) : that.getDomain() != null);

    }

    @Override
    public int hashCode() {
        int result = getAccountId() != null ? getAccountId().hashCode() : 0;
        result = 31 * result + (getUsername() != null ? getUsername().hashCode() : 0);
        result = 31 * result + (getDomain() != null ? getDomain().hashCode() : 0);
        return result;
    }
}

package org.stellar.sdk.federation;

import org.stellar.sdk.Config;

class FederationRequest {
    public final String name;
    public final String domain;

    public FederationRequest(String name, String domain) {
        this.name = name;
        this.domain = domain;
    }

    public static FederationRequest getFromValue(final String value) {
        final String[] tokens = value.split("@");
        if (tokens.length > 2)
            throw new MalformedAddressException();
        final String domain = tokens.length == 2 ? tokens[1] : Config.DEFAULT_DOMAIN;
        return new FederationRequest(tokens[0], domain);
    }
}

package org.stellar.sdk.federation;

import com.google.gson.annotations.SerializedName;

/**
 * Object to hold a response from a federation server.
 */
public class FederationResponse {
  @SerializedName("data")
  private final FederationData federationData;
  @SerializedName("success")
  private final boolean isSuccess;
  @SerializedName("error")
  private final String error;

  public FederationResponse() {
    this.federationData = new FederationData();
    isSuccess = false;
    error = null;
  }

  public boolean isSuccess() {
    return isSuccess;
  }

  public String getError() {
    return error;
  }

  public String getAccountId() {
    return this.federationData.getAccountId();
  }

  public String getUsername() {
    return this.federationData.getUsername();
  }

  public String getDomain() {
    return this.federationData.getDomain();
  }

  public void setAccountId(String address) {
    this.federationData.setAccountId(address);
  }

  public void setUsername(String username) {
    this.federationData.setUsername(username);
  }

  public void setDomain(String domain) {
    this.federationData.setDomain(domain);
  }

  public String getFirstName() {
    return this.federationData.getFirstName();
  }

  public String getLastName() {
    return this.federationData.getLastName();
  }

  public boolean isApproved() {
    return this.federationData.isApproved();
  }

  public String getName() {return this.federationData.getName(); }

  @Override
  public String toString() {
    return "FederationResponse{" +
            "federationData=" + federationData +
            ", isSuccess=" + isSuccess +
            ", error='" + error + '\'' +
            '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof FederationResponse)) return false;

    FederationResponse response = (FederationResponse) o;

    if (isSuccess() != response.isSuccess()) return false;
    if (federationData != null ? !federationData.equals(response.federationData) : response.federationData != null)
      return false;
    return !(getError() != null ? !getError().equals(response.getError()) : response.getError() != null);

  }

  @Override
  public int hashCode() {
    int result = federationData != null ? federationData.hashCode() : 0;
    result = 31 * result + (isSuccess() ? 1 : 0);
    result = 31 * result + (getError() != null ? getError().hashCode() : 0);
    return result;
  }
}

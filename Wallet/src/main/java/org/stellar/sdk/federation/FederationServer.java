package org.stellar.sdk.federation;

import com.google.common.net.InternetDomainName;
import com.google.gson.reflect.TypeToken;
import org.apache.http.client.utils.URIBuilder;
import org.stellar.sdk.Config;
import org.stellar.sdk.ConnectionErrorException;
import org.stellar.sdk.HttpClient;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * FederationServer handles a network connection to a
 * <a href="https://www.stellar.org/developers/learn/concepts/federation.html" target="_blank">federation server</a>
 * instance and exposes an interface for requests to that instance.
 *
 * For resolving a stellar address without knowing which federation server
 * to query use {@link Federation#resolve(String)}.
 *
 * @see <a href="https://www.stellar.org/developers/learn/concepts/federation.html" target="_blank">Federation docs</a>
 */
class FederationServer {
  private final URI serverUri;
  private final InternetDomainName domain;

  /**
   * Creates a new <code>FederationServer</code> instance.
   * @param serverUri Federation Server URI
   * @param domain Domain name this federation server is responsible for
   * @throws FederationServerInvalidException
   */
  public FederationServer(URI serverUri, InternetDomainName domain) {
    this.serverUri = serverUri;
    this.domain = domain;
  }

  /**
   * Creates a new <code>FederationServer</code> instance.
   * @param serverUri Federation Server URI
   * @param domain Domain name this federation server is responsible for
   * @throws FederationServerInvalidException
   */
  public FederationServer(String serverUri, InternetDomainName domain) {
    try {
      this.serverUri = new URI(serverUri);
    } catch (URISyntaxException e) {
      throw new FederationServerInvalidException();
    }
    this.domain = domain;
  }

  /**
   * Creates a <code>FederationServer</code> instance for a given domain.
   * It tries to find a federation server URL in db.
   * @param domain Domain to find a federation server for
   * @throws ConnectionErrorException
   * @throws NoFederationServerException
   * @throws FederationServerInvalidException
   */
  public static FederationServer createForDomain(InternetDomainName domain) {
    String federationServer = FederationAddressProvider.getURI(domain);
    if (federationServer == null) {
      throw new NoFederationServerException();
    }
    return new FederationServer(federationServer, domain);
  }

  /**
   * Resolves a username using a given federation server.
   * @throws ConnectionErrorException
   * @throws NotFoundException
   * @throws ServerErrorException
   */
  public FederationResponse resolveUsername(final FederationRequest request) {
    final URIBuilder uriBuilder = new URIBuilder(this.serverUri);
    uriBuilder.setPath("/federation/reverse");
    uriBuilder.setParameter("address", request.name);
    uriBuilder.setParameter("domain", request.domain);
    final URI uri = buildURI(uriBuilder);
    final FederationResponse response = federationRequest(uri);
    if (response != null && response.isSuccess())
      response.setAccountId(request.name);
    return response;
  }

  private URI buildURI(final URIBuilder builder) {
    try {
      return builder.build();
    } catch (URISyntaxException e) {
      throw new RuntimeException(e);
    }
  }

  private FederationResponse federationRequest(final URI uri) {
    return HttpClient.executeGetRequest(uri, TypeToken.get(FederationResponse.class));
  }

  /**
   * Resolves a stellar address using a given federation server.
   * @param request, like <code>bob@stellar.org</code>
   * @throws MalformedAddressException
   * @throws ConnectionErrorException
   * @throws NotFoundException
   * @throws ServerErrorException
   */
  public FederationResponse resolveAddress(FederationRequest request) {
    final URIBuilder uriBuilder = new URIBuilder(this.serverUri);
    uriBuilder.setPath("/federation");
    uriBuilder.setParameter("username", request.name);
    uriBuilder.setParameter("domain", request.domain);
    final URI uri = buildURI(uriBuilder);
    final FederationResponse response = federationRequest(uri);
    if (response != null && response.isSuccess()) {
      response.setUsername(request.name);
      response.setDomain(request.domain);
    }
    return response;
  }

  /**
   * Returns a federation server URI.
   */
  public URI getServerUri() {
    return serverUri;
  }

  /**
   * Returns a domain this server is responsible for.
   */
  public InternetDomainName getDomain() {
    return domain;
  }
}

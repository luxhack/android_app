package org.stellar.sdk.federation;

/**
 * Data not found by federation server
 */
public class NotFoundException extends RuntimeException {

    public NotFoundException(String message) {
        super(message);
    }

    public NotFoundException() {}

    public NotFoundException(Exception ex) {
        super(ex);
    }
}

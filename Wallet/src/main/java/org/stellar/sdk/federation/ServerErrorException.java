package org.stellar.sdk.federation;

/**
 * Server responsed with error
 */
public class ServerErrorException extends RuntimeException {

    public ServerErrorException() {}
    public ServerErrorException(Exception ex) {
        super(ex);
    }
    public ServerErrorException(final String ex) { super(ex);}
}

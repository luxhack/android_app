package org.stellar.sdk.requests;

import com.google.gson.reflect.TypeToken;
import org.stellar.sdk.KeyPair;
import org.stellar.sdk.responses.AccountResponse;

import java.io.IOException;
import java.net.URI;

/**
 * Builds requests connected to accounts.
 */
public class AccountsRequestBuilder extends IterableRequestBuilder<AccountResponse> {
  public AccountsRequestBuilder(URI serverURI) {
    super(serverURI, "accounts", TypeToken.get(AccountResponse.class));
  }

  /**
   * Requests <code>GET /accounts/{account}</code>
   * @see <a href="https://www.stellar.org/developers/horizon/reference/accounts-single.html">Account Details</a>
   * @param account Account to fetch
   * @throws IOException
   */
  public AccountResponse account(KeyPair account) throws IOException {
    this.setSegments("accounts", account.getAccountId());
    return this.execute();
  }

  public String accountBalance(KeyPair account) throws IOException {
    this.setSegments("accounts", account.getAccountId());
    return this.executeJsonRequest();
  }

  public String accountPayments(KeyPair account) {
    String paymentsSegment = "payments";
    this.setSegments("accounts", account.getAccountId(), paymentsSegment);
    this.limit(200);
    return this.executeJsonRequest();
  }

  public String paymentTime(String txId) {
    this.setSegments("transactions", txId);
    return this.executeJsonRequest();
  }

  @Override
  public AccountsRequestBuilder cursor(String token) {
    super.cursor(token);
    return this;
  }

  @Override
  public AccountsRequestBuilder limit(int number) {
    super.limit(number);
    return this;
  }

  @Override
  public AccountsRequestBuilder order(Order direction) {
    super.order(direction);
    return this;
  }
}

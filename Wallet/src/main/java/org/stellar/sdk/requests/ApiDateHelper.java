package org.stellar.sdk.requests;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ApiDateHelper {

    private static final DateFormat[] supported = new DateFormat[] {
            new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS"),
            new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
    };

    public static Date tryParseDate(final String strDate) {
        if (strDate == null || strDate.isEmpty())
            return new Date();
        for (final DateFormat format : supported) {
            try {
                return format.parse(strDate);
            } catch (ParseException ignored) {
            }
        }
        return new Date();
    }

}

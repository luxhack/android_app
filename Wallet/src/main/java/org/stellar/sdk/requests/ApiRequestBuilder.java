package org.stellar.sdk.requests;

import com.google.gson.reflect.TypeToken;
import org.apache.http.client.utils.URIBuilder;
import org.stellar.sdk.HttpClient;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;

abstract class ApiRequestBuilder<T> extends RequestBuilder {
    protected final TypeToken<T> responseType;

    ApiRequestBuilder(URI serverURI, String defaultSegment, TypeToken<T> responseType) {
        super(serverURI, defaultSegment);
        this.responseType = responseType;
    }

    /*
       * @throws TooManyRequestsException when too many requests were sent to the Horizon server.
       * @throws IOException
     */
    public T execute() throws IOException, TooManyRequestsException {
        return HttpClient.executeGetRequest(this.buildUri(), responseType);
    }

    public String executeJsonRequest() {
        return HttpClient.executeJsonGetRequest(this.buildUri());
    }

}

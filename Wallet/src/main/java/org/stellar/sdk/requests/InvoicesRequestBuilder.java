package org.stellar.sdk.requests;


import android.net.Uri;

import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;
import org.stellar.sdk.AssetTypeCreditAlphaNum;
import org.stellar.sdk.HttpClient;
import org.stellar.sdk.KeyPair;
import org.stellar.sdk.federation.ServerErrorException;
import org.stellar.sdk.responses.ApiResponse;
import org.stellar.sdk.responses.Invoice;
import org.stellar.sdk.responses.InvoiceResponse;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;

public class InvoicesRequestBuilder extends IterableRequestBuilder<ApiResponse<ArrayList<Invoice>>>{

    public InvoicesRequestBuilder(URI serverURI) {
        super(serverURI, "invoices/incoming/unpaid", new TypeToken<ApiResponse<ArrayList<Invoice>>>(){});
    }

    public ApiResponse<Invoice> create(final KeyPair seller, final KeyPair buyer, final AssetTypeCreditAlphaNum asset,
                       final String amount, final String note, final int code) {
        final Invoice invoice = new Invoice(seller, buyer, amount, asset, note, code);
        this.setSegments("invoices/create");
        return HttpClient.executeJsonPostRequest(this.buildUri(), invoice, new TypeToken<ApiResponse<Invoice>>(){});
    }

    public InvoiceResponse createInvoice(final String amount, final String accountId) {
        this.setSegments("invoices/");
        Map<String, String> invoiceData = new HashMap<>();
        invoiceData.put("accountId", accountId);
        invoiceData.put("amount", amount);
        JSONObject invoiceJSONObject = new JSONObject(invoiceData);

        URI uri = this.buildUri();
        String invoiceJSONString = invoiceJSONObject.toString();

        return HttpClient.executeJsonPostRequest(uri, invoiceJSONString,
                TypeToken.get(InvoiceResponse.class));
    }

    public InvoiceResponse getInvoice(int invoiceNumber){
        this.setSegments("invoices/" + invoiceNumber);
        URI uri = this.buildUri();
        return HttpClient.executeGetRequest(uri, TypeToken.get(InvoiceResponse.class));
    }

    public List<InvoiceResponse> forAccount(final KeyPair account) throws IOException {
        checkNotNull(account, "account cannot be null");
        this.setSegments("/accounts/" + account.getAccountId() + "/invoices");
        URI uri = this.buildUri();
        return HttpClient.executeGetRequest(uri, new TypeToken<ArrayList<InvoiceResponse>>(){});
    }

    public Invoice reject(final long invoiceId) {
        this.setSegments("invoices/reject");
        final ApiResponse<Invoice> response = HttpClient.executeJsonPostRequest(this.buildUri(),
                new InvoiceRejectRequest(invoiceId),
                new TypeToken<ApiResponse<Invoice>>(){});
        return response == null || !response.isSuccess() ? null : response.getUser();
    }

    public Invoice submitPayment(final long invoiceId, final String paymentHash) {
        this.setSegments("invoices/submitPayment");
        final ApiResponse<Invoice> response = HttpClient.executeJsonPostRequest(this.buildUri(),
                new SubmitPaymentRequest(paymentHash, invoiceId),
                new TypeToken<ApiResponse<Invoice>>(){});
        return response == null || !response.isSuccess() ? null : response.getUser();
    }

    private class SubmitPaymentRequest {
        @SerializedName("transactionId")
        private final String transactionId;
        @SerializedName("invoiceId")
        private final long invoiceId;

        public SubmitPaymentRequest(String transactionId, long invoiceId) {
            this.transactionId = transactionId;
            this.invoiceId = invoiceId;
        }

        public String getTransactionId() {
            return transactionId;
        }

        public long getInvoiceId() {
            return invoiceId;
        }
    }

    private class InvoiceRejectRequest {
        @SerializedName("id")
        private final long id;

        public InvoiceRejectRequest(long id) {
            this.id = id;
        }

        public long getId() {
            return id;
        }
    }
}

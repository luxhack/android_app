package org.stellar.sdk.requests;

import com.google.gson.reflect.TypeToken;

import java.net.URI;

/**
 * Abstract class for request builders.
 */
abstract class IterableRequestBuilder<T> extends ApiRequestBuilder<T> {

  IterableRequestBuilder(URI serverURI, String defaultSegment, TypeToken<T> responseType) {
    super(serverURI, defaultSegment, responseType);
  }

  /**
   * Sets <code>cursor</code> parameter on the request.
   * A cursor is a value that points to a specific location in a collection of resources.
   * The cursor attribute itself is an opaque value meaning that users should not try to parse it.
   * @see <a href="https://www.stellar.org/developers/horizon/reference/resources/page.html">Page documentation</a>
   * @param cursor
   */
  public IterableRequestBuilder cursor(String cursor) {
    uriBuilder.addParameter("cursor", cursor);
    return this;
  }

  /**
   * Sets <code>limit</code> parameter on the request.
   * It defines maximum number of records to return.
   * For range and default values check documentation of the endpoint requested.
   * @param number maxium number of records to return
   */
  public IterableRequestBuilder limit(int number) {
    uriBuilder.addParameter("limit", String.valueOf(number));
    return this;
  }

  /**
   * Sets <code>order</code> parameter on the request.
   * @param direction {@link IterableRequestBuilder.Order}
   */
  public IterableRequestBuilder order(Order direction) {
    uriBuilder.addParameter("order", direction.getValue());
    return this;
  }

  /**
   * Represents possible <code>order</code> parameter values.
   */
  public enum Order {
    ASC("asc"),
    DESC("desc");
    private final String value;
    Order(String value) {
      this.value = value;
    }
    public String getValue() {
      return value;
    }
  }
}

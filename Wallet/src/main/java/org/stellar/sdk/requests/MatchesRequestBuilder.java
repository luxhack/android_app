package org.stellar.sdk.requests;

import com.google.gson.reflect.TypeToken;
import org.stellar.sdk.Asset;
import org.stellar.sdk.AssetTypeCreditAlphaNum;
import org.stellar.sdk.KeyPair;
import org.stellar.sdk.responses.MatchResponse;
import org.stellar.sdk.responses.Page;

import java.net.URI;

public class MatchesRequestBuilder extends IterableRequestBuilder<Page<MatchResponse>> {
    public MatchesRequestBuilder(URI serverURI) {
        super(serverURI, "/order_book/trades", new TypeToken<Page<MatchResponse>>(){});
    }

    public MatchesRequestBuilder buyingAsset(Asset asset) {
        return this.asset(asset, true);
    }

    public MatchesRequestBuilder sellingAsset(Asset asset) {
        return this.asset(asset, false);
    }

    public MatchesRequestBuilder forAccount(final KeyPair account) {
        this.setSegments("accounts", account.getAccountId(), "trades");
        return this;
    }

    private MatchesRequestBuilder asset(Asset asset, boolean isBuying) {
        final String suffix = isBuying ? "buying_" : "selling_";
        this.uriBuilder.addParameter(suffix + "asset_type", asset.getType());
        if (asset instanceof AssetTypeCreditAlphaNum) {
            AssetTypeCreditAlphaNum creditAlphaNumAsset = (AssetTypeCreditAlphaNum) asset;
            uriBuilder.addParameter(suffix + "asset_code", creditAlphaNumAsset.getCode());
            uriBuilder.addParameter(suffix + "asset_issuer", creditAlphaNumAsset.getIssuer().getAccountId());
        }
        return this;
    }

    @Override
    public MatchesRequestBuilder cursor(String token) {
        super.cursor(token);
        return this;
    }

    @Override
    public MatchesRequestBuilder limit(int number) {
        super.limit(number);
        return this;
    }

    @Override
    public MatchesRequestBuilder order(Order direction) {
        super.order(direction);
        return this;
    }
}
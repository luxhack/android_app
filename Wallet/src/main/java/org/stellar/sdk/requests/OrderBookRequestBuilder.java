package org.stellar.sdk.requests;

import com.google.gson.reflect.TypeToken;
import org.stellar.sdk.Asset;
import org.stellar.sdk.AssetTypeCreditAlphaNum;
import org.stellar.sdk.responses.OrderBookResponse;

import java.net.URI;

/**
 * Builds requests connected to order book.
 */
public class OrderBookRequestBuilder extends ApiRequestBuilder<OrderBookResponse> {
  public OrderBookRequestBuilder(URI serverURI) {
    super(serverURI, "order_book", TypeToken.get(OrderBookResponse.class));
  }

  public OrderBookRequestBuilder buyingAsset(Asset asset) {
    return this.setAssetData(asset, true);
  }
  
  public OrderBookRequestBuilder sellingAsset(Asset asset) {
    return this.setAssetData(asset, false);
  }

  private OrderBookRequestBuilder setAssetData(Asset asset, boolean isBuy) {
    final String suffix = isBuy ? "buying_" : "selling_";
    uriBuilder.addParameter(suffix + "asset_type", asset.getType());
    if (asset instanceof AssetTypeCreditAlphaNum) {
      AssetTypeCreditAlphaNum creditAlphaNumAsset = (AssetTypeCreditAlphaNum) asset;
      uriBuilder.addParameter(suffix + "asset_code", creditAlphaNumAsset.getCode());
      uriBuilder.addParameter(suffix + "asset_issuer", creditAlphaNumAsset.getIssuer().getAccountId());
    }
    return this;
  }
}

package org.stellar.sdk.requests;

import com.google.gson.reflect.TypeToken;
import org.stellar.sdk.Asset;
import org.stellar.sdk.AssetTypeCreditAlphaNum;
import org.stellar.sdk.KeyPair;
import org.stellar.sdk.responses.AccountResponse;
import org.stellar.sdk.responses.Invoice;
import org.stellar.sdk.responses.Page;
import org.stellar.sdk.responses.PathResponse;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.URI;
import java.util.*;

/**
 * Builds requests connected to paths.
 */
public class PathsRequestBuilder extends IterableRequestBuilder<Page<PathResponse>> {
  private AccountResponse sourceAccountResponse;
  private KeyPair sourceAccount;

  public PathsRequestBuilder(URI serverURI) {
    super(serverURI, "paths", new TypeToken<Page<PathResponse>>(){});
  }

  public PathsRequestBuilder destinationAccount(KeyPair account) {
    uriBuilder.addParameter("destination_account", account.getAccountId());
    return this;
  }

  public PathsRequestBuilder forInvoice(Invoice invoice) {
    return this.sourceAccount(invoice.getBuyer())
      .destinationAccount(invoice.getSeller())
      .destinationAsset(invoice.getAsset())
      .destinationAmount(invoice.getAmount());
  }

  public PathsRequestBuilder sourceAccount(KeyPair account) {
    this.sourceAccount = account;
    return this;
  }

  public PathsRequestBuilder sourceAccount(AccountResponse account) {
    this.sourceAccountResponse = account;
    this.sourceAccount(account.getKeypair());
    return this;
  }

  public PathsRequestBuilder destinationAmount(String amount) {
    uriBuilder.addParameter("destination_amount", amount);
    return this;
  }

  @Override
  public Page<PathResponse> execute() throws IOException, TooManyRequestsException {
    uriBuilder.addParameter("source_account", this.sourceAccount.getAccountId());
    AccountResponse.Balance[] balances = getBalancesList();
    Page<PathResponse> paths = super.execute();
    ArrayList<PathResponse> filtered = filter(paths.getRecords(), balances);
    return new Page<PathResponse>(filtered, paths.getLinks(), this.responseType);
  }

  private static ArrayList<PathResponse> filter(List<PathResponse> paths, AccountResponse.Balance[] rawBalances) {
    if (paths == null || rawBalances == null)
      return new ArrayList<PathResponse>();
    final Map<Asset, BigDecimal> balances = getBalances(rawBalances);
    final ArrayList<PathResponse> filtered = new ArrayList<PathResponse>();
    for (final PathResponse path : paths) {
      final BigDecimal balance = balances.get(path.getSourceAsset());
      if (balance == null)
        continue;
      if (path.getSourceAmountDecimal().compareTo(balance) > 0)
        continue;
      filtered.add(path);
    }
    return filtered;
  }

  private static Map<Asset, BigDecimal> getBalances(AccountResponse.Balance[] balances) {
    final Map<Asset, BigDecimal> map = new HashMap<Asset, BigDecimal>();
    for (AccountResponse.Balance b : balances) map.put(b.getAsset(), b.getDecimalBalance());
    return map;
  }

  private AccountResponse.Balance[] getBalancesList() throws IOException {
    if (this.sourceAccountResponse != null)
      return this.sourceAccountResponse.getBalances();
    final AccountResponse account = new AccountsRequestBuilder(this.baseURI).account(this.sourceAccount);
    return account == null ? new AccountResponse.Balance[0] : account.getBalances();
  }

  public PathsRequestBuilder destinationAsset(Asset asset) {
    uriBuilder.addParameter("destination_asset_type", asset.getType());
    if (asset instanceof AssetTypeCreditAlphaNum) {
      AssetTypeCreditAlphaNum creditAlphaNumAsset = (AssetTypeCreditAlphaNum) asset;
      uriBuilder.addParameter("destination_asset_code", creditAlphaNumAsset.getCode());
      uriBuilder.addParameter("destination_asset_issuer", creditAlphaNumAsset.getIssuer().getAccountId());
    }
    return this;
  }
}

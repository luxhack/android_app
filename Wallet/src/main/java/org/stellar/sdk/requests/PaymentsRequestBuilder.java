package org.stellar.sdk.requests;

import com.google.gson.reflect.TypeToken;
import org.stellar.sdk.HttpClient;
import org.stellar.sdk.KeyPair;
import org.stellar.sdk.responses.Page;
import org.stellar.sdk.responses.operations.OperationResponse;
import org.stellar.sdk.responses.operations.PaymentOperationResponse;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Builds requests connected to payments.
 */
public class PaymentsRequestBuilder extends IterableRequestBuilder<Page<PaymentOperationResponse>> {
  public PaymentsRequestBuilder(URI serverURI) {
    super(serverURI, "payments", new TypeToken<Page<PaymentOperationResponse>>(){});
  }

  /**
   * Builds request to <code>GET /accounts/{account}/payments</code>
   * @see <a href="https://www.stellar.org/developers/horizon/reference/payments-for-account.html">Payments for Account</a>
   * @param account Account for which to get payments
   */
  public PaymentsRequestBuilder forAccountExt(KeyPair account) {
    account = checkNotNull(account, "account cannot be null");
    this.setSegments("accounts", account.getAccountId(), "payments");
    return this;
  }

  public List<PaymentOperationResponse> forAccount(KeyPair account, final int limit) {
      this.limit(limit);
      return this.forAccountExt(account).execute().getRecords();
  }

  public List<PaymentOperationResponse> forAccount(KeyPair account) {
    final int defaultLimit = 100;
    return this.forAccount(account, defaultLimit);
  }

  /**
   * Builds request to <code>GET /ledgers/{ledgerSeq}/payments</code>
   * @see <a href="https://www.stellar.org/developers/horizon/reference/payments-for-ledger.html">Payments for Ledger</a>
   * @param ledgerSeq Ledger for which to get payments
   */
  public PaymentsRequestBuilder forLedger(long ledgerSeq) {
    this.setSegments("ledgers", String.valueOf(ledgerSeq), "payments");
    return this;
  }

  @Override
  public Page<PaymentOperationResponse> execute() {
    Page<OperationResponse> response = HttpClient.executeGetRequest(this.buildUri(), new TypeToken<Page<OperationResponse>>(){});
    ArrayList<PaymentOperationResponse> payments = filterZero(response.getRecords());
    return new Page<PaymentOperationResponse>(payments, response.getLinks(), this.responseType);
  }

  ArrayList<PaymentOperationResponse> filterZero(List<OperationResponse> operations) {
    if (operations == null)
      return null;
    ArrayList<PaymentOperationResponse> filteredOperations = new ArrayList<PaymentOperationResponse>();
    for (OperationResponse operation : operations) {
      if (!(operation instanceof PaymentOperationResponse))
        continue;
      final PaymentOperationResponse payment = (PaymentOperationResponse) operation;
      if (Double.parseDouble(payment.getAmount()) == 0)
        continue;
      filteredOperations.add(payment);
    }
    return filteredOperations;
  }

  /**
   * Builds request to <code>GET /transactions/{transactionId}/payments</code>
   * @see <a href="https://www.stellar.org/developers/horizon/reference/payments-for-transaction.html">Payments for Transaction</a>
   * @param transactionId Transaction ID for which to get payments
   */
  public PaymentsRequestBuilder forTransaction(String transactionId) {
    transactionId = checkNotNull(transactionId, "transactionId cannot be null");
    this.setSegments("transactions", transactionId, "payments");
    return this;
  }

  @Override
  public PaymentsRequestBuilder cursor(String token) {
    super.cursor(token);
    return this;
  }

  @Override
  public PaymentsRequestBuilder limit(int number) {
    super.limit(number);
    return this;
  }

  @Override
  public PaymentsRequestBuilder order(Order direction) {
    super.order(direction);
    return this;
  }
}
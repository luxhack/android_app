package org.stellar.sdk.requests;

import android.util.Log;

import org.apache.http.client.utils.URIBuilder;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;

public class RequestBuilder{
    protected final URIBuilder uriBuilder;
    protected final ArrayList<String> segments;
    protected URI baseURI;
    protected boolean segmentsAdded;

    public RequestBuilder(URI serverURI, String defaultSegment) {
        this.baseURI = serverURI;
        uriBuilder = new URIBuilder(this.baseURI);
        segments = new ArrayList<String>();
        if (defaultSegment != null) {
            this.setSegments(defaultSegment);
        }
        segmentsAdded = false; // Allow overwriting segments
    }

    protected RequestBuilder setSegments(String... segments) {
        if (segmentsAdded) {
            throw new RuntimeException("URL segments have been already added.");
        }

        segmentsAdded = true;
        // Remove default segments
        this.segments.clear();
        Collections.addAll(this.segments, segments);
        return this;
    }

    URI buildUri() {
        if (segments.size() > 0) {
            String path = "";
            for (String segment : segments) {
                path += "/"+segment;
            }
            uriBuilder.setPath(path);
        }
        try {
            return uriBuilder.build();
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    URI buildWalletsUri() {
        URI uri = null;
        try {
            //URL url = new URL("http://37.139.22.108:3001/wallets/show_login_params");
            URL url = new URL("http://37.139.22.108:3001");
            uri = url.toURI();
        } catch (URISyntaxException e) {
            Log.e("getWallet", e.getMessage());
        } catch (MalformedURLException e) {
            Log.e("getWallet", e.getMessage());
        }
        Log.d("WallerRequestBuilder", "URI: " + uri.toString());
        return uri;
    }
}

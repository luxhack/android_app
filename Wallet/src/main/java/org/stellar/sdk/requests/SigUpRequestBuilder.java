package org.stellar.sdk.requests;

import com.google.gson.reflect.TypeToken;
import org.stellar.sdk.HttpClient;
import org.stellar.sdk.KeyPair;
import org.stellar.sdk.responses.UserInfo;

import java.net.URI;

public class SigUpRequestBuilder extends ApiRequestBuilder<UserInfo> {

    public SigUpRequestBuilder(URI serverURI) {
        super(serverURI, "users/register", TypeToken.get(UserInfo.class));
    }

    public UserInfo signUp(String firstName, String lastName, String email, String phoneNumber, String login,
                           String password, KeyPair account) {
        final URI uri = this.buildUri();
        final SignUpRequest request = new SignUpRequest(firstName, lastName, email, phoneNumber, login, password,
                URI.create(uri.getHost()), account);
        final UserInfo response = HttpClient.executeJsonPostRequest(uri, request, this.responseType);
        return response;
    }
}

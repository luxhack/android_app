package org.stellar.sdk.requests;

import com.google.gson.annotations.SerializedName;
import org.stellar.sdk.Config;
import org.stellar.sdk.KeyPair;
import org.stellar.sdk.wallet.WalletSaveRequest;
import org.stellar.sdk.wallet.WalletSaver;

import java.net.URI;

public class SignUpRequest {
    private static class User {
        @SerializedName("firstName")
        private final String firstName;
        @SerializedName("lastName")
        private final String lastName;
        @SerializedName("email")
        private final String email;
        @SerializedName("phoneNumber")
        private final String phoneNumber;
        @SerializedName("login")
        private final String login;
        @SerializedName("accountId")
        private final String accountId;

        public User(String firstName, String lastName, String email, String phoneNumber, String login, String accountId) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.email = email;
            this.phoneNumber = phoneNumber;
            this.login = login;
            this.accountId = accountId;
        }

        public String getFirstName() {
            return firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public String getEmail() {
            return email;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public String getLogin() {
            return login;
        }

        public String getAccountId() {
            return accountId;
        }
    }

    @SerializedName("user")
    private final User user;
    @SerializedName("wallet")
    private final WalletSaveRequest wallet;
    public SignUpRequest(String firstName, String lastName, String email, String phoneNumber,
                         String login, String password, final URI storageUri, KeyPair account) {
        String userName, domain;
        final String[] tokens = login.split("@");
        if (tokens.length == 1) {
            userName = tokens[0];
            domain = Config.DEFAULT_DOMAIN;
        } else if (tokens.length == 2) {
            userName = tokens[0];
            domain = tokens[1];
        } else {
            throw new IllegalArgumentException("Login has invalid format!");
        }
        this.user = new User(firstName, lastName, email, phoneNumber, userName, account.getAccountId());
        //String username, String walletId, String salt, String publicKey, String mainData,
        //String keychainData, String kdfParams
        this.wallet = WalletSaver.createEncryptedWallet(userName + "@" + domain, password, account, storageUri);
    }

    public User getUser() {
        return user;
    }

    public WalletSaveRequest getWallet() {
        return wallet;
    }
}

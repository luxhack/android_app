package org.stellar.sdk.requests;

import com.google.gson.reflect.TypeToken;
import org.stellar.sdk.responses.TransactionResponse;

import java.io.IOException;
import java.net.URI;

public class TransactionRequestBuilder extends ApiRequestBuilder<TransactionResponse> {
    public TransactionRequestBuilder(URI serverURI) {
        super(serverURI, "transactions", TypeToken.get(TransactionResponse.class));
    }

    /**
     * Requests <code>GET /transactions/{transactionId}</code>
     * @see <a href="https://www.stellar.org/developers/horizon/reference/transactions-single.html">Transaction Details</a>
     * @param transactionId Transaction to fetch
     * @throws IOException
     */
    public TransactionResponse transaction(String transactionId) throws IOException {
        this.setSegments("transactions", transactionId);
        return this.execute();
    }
}

package org.stellar.sdk.requests;

import com.google.gson.reflect.TypeToken;
import org.stellar.sdk.HttpClient;
import org.stellar.sdk.KeyPair;
import org.stellar.sdk.responses.UserInfo;

import java.net.URI;

public class UserInfoRequestBuilder extends ApiRequestBuilder<UserInfo> {

    public UserInfoRequestBuilder(URI serverURI) {
        super(serverURI, "users", TypeToken.get(UserInfo.class));
    }

    public UserInfo getInfo(final int id) {
        this.uriBuilder.addParameter("id", Integer.toString(id));
        final URI uri = this.buildUri();
        final UserInfo response = HttpClient.executeGetRequest(uri, this.responseType);
        return response;
    }
}

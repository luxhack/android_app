package org.stellar.sdk.requests;


import com.google.gson.reflect.TypeToken;

import org.apache.http.entity.StringEntity;
import org.stellar.sdk.Config;
import org.stellar.sdk.HttpClient;
import org.stellar.sdk.KeyPair;
import org.stellar.sdk.federation.ServerErrorException;
import org.stellar.sdk.responses.GsonSingleton;
import org.stellar.sdk.responses.WalletCreateResponse;
import org.stellar.sdk.wallet.WalletSaveRequest;
import org.stellar.sdk.wallet.WalletSaver;

import java.io.UnsupportedEncodingException;
import java.net.URI;

public class WalletCreateRequestBuilder extends ApiRequestBuilder<WalletCreateResponse> {

    public WalletCreateRequestBuilder(URI serverURI) {
        super(serverURI, "/v2/wallets/create", TypeToken.get(WalletCreateResponse.class));
    }

    public WalletCreateResponse createWallet(String login, String password, KeyPair account){
        final URI uri = this.buildUri();String userName, domain;
        final String[] tokens = login.split("@");
        if (tokens.length == 1) {
            userName = tokens[0];
            domain = Config.DEFAULT_DOMAIN;
        } else if (tokens.length == 2) {
            userName = tokens[0];
            domain = tokens[1];
        } else {
            throw new IllegalArgumentException("Login has invalid format!");
        }
        final WalletSaveRequest request = WalletSaver.createEncryptedWallet(userName + "@" + domain, password, account, URI.create(uri.getHost()));

        final String serializedJson = GsonSingleton.getInstance().toJson(request);
        StringEntity entity;
        try {
            entity = new StringEntity(serializedJson);
            entity.setContentType("application/json");
        } catch (UnsupportedEncodingException e) {
            throw new ServerErrorException(e);
        }

        final WalletCreateResponse response = HttpClient.executePostRequest(uri, entity, this.responseType, true);
        return response;
    }
}

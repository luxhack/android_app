package org.stellar.sdk.requests;

import android.util.Log;

import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;
import org.stellar.sdk.KeyPair;
import org.stellar.sdk.wallet.WalletManager;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

public class WalletRequestBuilder extends ApiRequestBuilder<WalletRequestBuilder.WalletResponse> {
    protected static class WalletResponse {
        @SerializedName("status")
        private final String status;

        public WalletResponse(String status) {
            this.status = status;
        }

        public String getStatus() {
            return status;
        }

        public boolean isSuccess() {
            return "success".equals(this.status);
        }
    }
    public WalletRequestBuilder(URI serverURI) {
        //super(serverURI, "v2/wallets", TypeToken.get(WalletResponse.class));
        super(serverURI, "", TypeToken.get(WalletResponse.class));
    }

    public KeyPair getWallet(final String login, final String password) {
        this.segments.clear();
        //final URI uri = this.buildUri();
        final URI uri = this.buildWalletsUri();
        Log.d("getWallet", uri.getPath());

        return WalletManager.getKeyPair(login, password, uri);
    }
}

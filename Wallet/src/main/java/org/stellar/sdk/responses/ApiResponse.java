package org.stellar.sdk.responses;


import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ApiResponse<T> {

    @SerializedName("success")
    private boolean success;
    @SerializedName("data")
    protected T data;
    @SerializedName("error")
    protected String error;

    public ApiResponse(boolean success) {
        this.success = success;
    }

    public boolean isSuccess() {
        return success;
    }

    public T getUser() {
        return data;
    }

    public String getError() {
        return error;
    }
}

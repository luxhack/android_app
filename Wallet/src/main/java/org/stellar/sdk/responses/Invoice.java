package org.stellar.sdk.responses;


import com.google.gson.annotations.SerializedName;
import org.stellar.sdk.Asset;
import org.stellar.sdk.AssetTypeCreditAlphaNum;
import org.stellar.sdk.KeyPair;
import org.stellar.sdk.requests.ApiDateHelper;

import java.util.Date;

import static com.google.common.base.Preconditions.checkNotNull;

public class Invoice {
    @SerializedName("id")
    private final long id;
    @SerializedName("seller")
    private final String sellerStr;
    @SerializedName("buyer")
    private final String buyerStr;
    @SerializedName("amount")
    private final String amount;
    @SerializedName("currency")
    private final String currency;
    @SerializedName("issuer")
    private final String issuer;
    @SerializedName("code")
    private final int code;
    @SerializedName("note")
    private final String note;
    @SerializedName("createdAt")
    private final String createdAt;

    private transient Date _createdAt;
    private transient KeyPair buyer;
    private transient KeyPair seller;

    public Invoice(KeyPair seller, KeyPair buyer, String amount, AssetTypeCreditAlphaNum asset, String note,
                   final int code) {
        this.seller = checkNotNull(seller, "seller cannot be null");
        this.sellerStr = this.seller.getAccountId();
        this.buyer = checkNotNull(buyer, "buyer cannot be null");
        this.buyerStr = this.buyer.getAccountId();
        this.amount = checkNotNull(amount, "amount cannot be null");
        asset = checkNotNull(asset, "asset cannot be null");
        this.currency = asset.getCode();
        this.issuer = asset.getIssuer().getAccountId();
        this.code = code;
        this.note = note == null ? "" : note;
        this.id = 0;
        this.createdAt = null;
    }

    private Asset asset;

    public long getId() {
        return id;
    }

    public KeyPair getSeller() {
        if (this.seller != null)
            return this.seller;
        return this.seller = KeyPair.tryFromAccountId(this.sellerStr);
    }

    public KeyPair getBuyer() {
        if (this.buyer != null)
            return this.buyer;
        return this.buyer = KeyPair.tryFromAccountId(this.buyerStr);
    }

    public String getAmount() {
        return amount;
    }

    public int getCode() {
        return code;
    }

    public String getNote() {
        return note;
    }

    public Asset getAsset() {
        if (asset == null) {
            asset = Asset.createNonNativeAsset(this.currency, KeyPair.fromAccountId(this.issuer));
        }
        return asset;
    }

    public Date getCreatedAt() {
        if (_createdAt == null) {
            _createdAt = ApiDateHelper.tryParseDate(this.createdAt);
        }
        return _createdAt;
    }

    @Override
    public String toString() {
        return "Invoice{" +
                "id=" + id +
                ", seller=" + this.getSeller() +
                ", buyer=" + this.getBuyer() +
                ", amount='" + this.getAmount() + '\'' +
                ", currency='" + currency + '\'' +
                ", issuer=" + issuer +
                ", code='" + code + '\'' +
                ", note='" + note + '\'' +
                ", createdAt=" + getCreatedAt() +
                ", asset=" + getAsset() +
                '}';
    }
}

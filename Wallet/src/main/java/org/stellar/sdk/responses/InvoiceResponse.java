package org.stellar.sdk.responses;

import com.google.gson.annotations.SerializedName;

/**
 * Created by andstepko on 10.05.16.
 */
public class InvoiceResponse {
    @SerializedName("accountId")
    private String accountId;
    @SerializedName("amount")
    private String amountString;
    @SerializedName("invoiceNumber")
    private int invoiceNumber;
    @SerializedName("createdAt")
    private String createdAt;

    public InvoiceResponse(String accountId, String amountString, int invoiceNumber, String createdAt) {
        this.accountId = accountId;
        this.amountString = amountString;
        this.invoiceNumber = invoiceNumber;
        this.createdAt = createdAt;
    }

    public String getAccountId() {
        return accountId;
    }

    public String getAmountString() {
        return amountString;
    }

    public int getInvoiceNumber() {
        return invoiceNumber;
    }

    public String getCreatedAt() {
        return createdAt;
    }
}

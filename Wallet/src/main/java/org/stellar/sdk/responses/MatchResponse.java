package org.stellar.sdk.responses;

import com.google.gson.annotations.SerializedName;
import org.stellar.sdk.Asset;
import org.stellar.sdk.KeyPair;

public class MatchResponse {
    @SerializedName("ID")
    private final String id;
    @SerializedName("paging_token")
    private final String pagingToken;
    @SerializedName("seller")
    private final String seller;
    @SerializedName("sold_asset_type")
    private final String soldAssetType;
    @SerializedName("sold_asset_code")
    private final String soldAssetCode;
    @SerializedName("sold_asset_issuer")
    private final String soldAssetIssuer;
    @SerializedName("sold_amount")
    private final String soldAmount;
    @SerializedName("buyer")
    private final String buyer;
    @SerializedName("bought_asset_type")
    private final String boughtAssetType;
    @SerializedName("bought_asset_code")
    private final String boughtAssetCode;
    @SerializedName("bought_asset_issuer")
    private final String boughtAssetIssuer;
    @SerializedName("bought_amount")
    private final String boughtAmount;


    private KeyPair kpSeller;
    private KeyPair kpBuyer;
    private Asset soldAsset;
    private Asset broughtAsset;

    public MatchResponse(String boughtAmount, String id, String pagingToken, String seller, String soldAssetType,
                         String soldAssetCode, String soldAssetIssuer, String soldAmount, String buyer,
                         String boughtAssetType, String boughtAssetCode, String boughtAssetIssuer) {
        this.boughtAmount = boughtAmount;
        this.id = id;
        this.pagingToken = pagingToken;
        this.seller = seller;
        this.soldAssetType = soldAssetType;
        this.soldAssetCode = soldAssetCode;
        this.soldAssetIssuer = soldAssetIssuer;
        this.soldAmount = soldAmount;
        this.buyer = buyer;
        this.boughtAssetType = boughtAssetType;
        this.boughtAssetCode = boughtAssetCode;
        this.boughtAssetIssuer = boughtAssetIssuer;
    }

    public String getId() {
        return id;
    }

    public String getPagingToken() {
        return pagingToken;
    }

    public Asset getBroughtAsset() {
        if (this.broughtAsset == null) {
            this.broughtAsset = Asset.createAsset(this.boughtAssetType, this.boughtAssetCode, this.boughtAssetIssuer);
        }
        return this.broughtAsset;
    }

    public Asset getSoldAsset() {
        if (this.soldAsset == null) {
            this.soldAsset = Asset.createAsset(this.soldAssetType, this.soldAssetCode, this.soldAssetIssuer);
        }
        return this.soldAsset;
    }

    public KeyPair getBuyer() {
        if (this.kpBuyer == null) {
            this.kpBuyer = KeyPair.fromAccountId(this.buyer);
        }
        return this.kpBuyer;
    }

    public KeyPair getSeller() {
        if (this.kpSeller == null) {
            this.kpSeller = KeyPair.fromAccountId(this.seller);
        }
        return  this.kpSeller;
    }

    public String getBoughtAmount() {
        return boughtAmount;
    }

    public String getSoldAmount() {
        return soldAmount;
    }
}

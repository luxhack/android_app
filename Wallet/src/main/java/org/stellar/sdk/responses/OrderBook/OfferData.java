package org.stellar.sdk.responses.OrderBook;

import com.google.gson.annotations.SerializedName;
import org.stellar.sdk.Price;

public class OfferData {
    @SerializedName("price_r")
    private final Price priceDetails;
    @SerializedName("price")
    private final String price;
    @SerializedName("amount")
    private final String amount;

    public OfferData(Price priceDetails, String price, String amount) {
        this.priceDetails = priceDetails;
        this.price = price;
        this.amount = amount;
    }

    public Price getPriceDetails() {
        return priceDetails;
    }

    public String getPrice() {
        return price;
    }

    public String getAmount() {
        return amount;
    }
}

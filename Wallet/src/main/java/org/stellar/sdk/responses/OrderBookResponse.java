package org.stellar.sdk.responses;


import com.google.gson.annotations.SerializedName;
import org.stellar.sdk.Asset;
import org.stellar.sdk.responses.OrderBook.OfferData;

public class OrderBookResponse extends Response {
    @SerializedName("bids")
    private final OfferData[] bids;
    @SerializedName("asks")
    private final OfferData[] asks;
    @SerializedName("base")
    private final Asset base;
    @SerializedName("counter")
    private final Asset counter;

    public OrderBookResponse(OfferData[] bids, OfferData[] asks, Asset base, Asset counter) {
        this.bids = bids;
        this.asks = asks;
        this.base = base;
        this.counter = counter;
    }

    public OfferData[] getBids() {
        return bids;
    }

    public OfferData[] getAsks() {
        return asks;
    }

    public Asset getBase() {
        return base;
    }

    public Asset getCounter() {
        return counter;
    }
}

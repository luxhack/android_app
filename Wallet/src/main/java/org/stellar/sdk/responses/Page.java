package org.stellar.sdk.responses;

import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import org.apache.http.client.fluent.Request;
import org.stellar.sdk.HttpClient;
import org.stellar.sdk.requests.ResponseHandler;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

/**
 * Represents page of objects.
 * @see <a href="https://www.stellar.org/developers/horizon/reference/resources/page.html" target="_blank">Page documentation</a>
 */
public class Page<T> extends Response {
    @SerializedName("records")
    private ArrayList<T> records;
    @SerializedName("links")
    private Links links;
    @SerializedName("_embedded")
    private EmbeddedPage<T> embeddedPage;
    @SerializedName("_links")
    private Links embeddedLinks;
    private TypeToken<Page<T>> type;

  public Page() {
     this(new TypeToken<Page<T>>() {});
  }

    public Page(final TypeToken<Page<T>> type) {
        this.type = type;
    }

    public Page(ArrayList<T> records, Links links, final TypeToken<Page<T>> type) {
        this(type);
        this.records = records;
        this.links = links;
    }

    public ArrayList<T> getRecords() {
    if (this.records == null && this.embeddedPage != null) {
      return this.embeddedPage.getRecords();
    }
    return records;
  }

    public void setRecords(ArrayList<T> records) {
        this.records = records;
    }

    public Links getLinks() {
    if (this.links == null && this.embeddedLinks != null) {
      return this.embeddedLinks;
    }
    return links;
  }

  /**
   * @return The next page of results or null when there is no more results
   * @throws IOException
   */
  public Page<T> getNextPage() throws IOException {
    if (this.getLinks().getNext() == null) {
      return null;
    }
      try {
          final URI uri = new URI(this.getLinks().getNext().getHref());
          final Page<T> response = HttpClient.executeGetRequest(uri, this.getType());
          if (response != null)
              response.type = this.getType();
          return response;
      } catch (URISyntaxException e) {
          return null;
      }
  }

    private TypeToken<Page<T>> getType() {
        if (this.type == null) {
            this.type = new TypeToken<Page<T>>(){};
        }
        return this.type;
    }

  /**
   * Links connected to page response.
   */
  public static class Links {
    @SerializedName("next")
    private final Link next;
    @SerializedName("prev")
    private final Link prev;
    @SerializedName("self")
    private final Link self;

    Links(Link next, Link prev, Link self) {
      this.next = next;
      this.prev = prev;
      this.self = self;
    }

    public Link getNext() {
      return next;
    }

    public Link getPrev() {
      return prev;
    }

    public Link getSelf() {
      return self;
    }
  }

    private static class EmbeddedPage<T> {
        @SerializedName("records")
        private ArrayList<T> records;

        public EmbeddedPage(ArrayList<T> records) {
            this.records = records;
        }

        public ArrayList<T> getRecords() {
            return records;
        }
    }
}

package org.stellar.sdk.responses;

import com.google.gson.annotations.SerializedName;

public class UserInfo extends ApiResponse<UserInfo.User> {
    public static class User {
        @SerializedName("firstName")
        private final String firstName;
        @SerializedName("lastName")
        private final String lastName;
        @SerializedName("email")
        private final String email;
        @SerializedName("phoneNumber")
        private final String phoneNumber;
        @SerializedName("login")
        private final String login;
        @SerializedName("accountId")
        private final String accountId;
        @SerializedName("walletId")
        private final int walletId;
        @SerializedName("id")
        private final int id;
        @SerializedName("state")
        private final String state;

        private UserState userState;

        public User(String firstName, String lastName, String email, String phoneNumber, String login,
                    String accountId, int walletId, int id, String state) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.email = email;
            this.phoneNumber = phoneNumber;
            this.login = login;
            this.accountId = accountId;
            this.walletId = walletId;
            this.id = id;
            this.state = state;
        }

        public String getFirstName() {
            return firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public String getEmail() {
            return email;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public String getLogin() {
            return login;
        }

        public String getAccountId() {
            return accountId;
        }

        public int getWalletId() {
            return walletId;
        }

        public int getId() {
            return id;
        }

        public UserState getState() {
            if (userState == null) {
                userState = UserState.toEnum(state);
            }
            return userState;
        }
    }

    public UserInfo(String firstName, String lastName, String email, String phoneNumber, String login,
                    String accountId, int walletId, int id, String state, boolean isSuccess) {
        super(isSuccess);
        this.data = new User(firstName, lastName, email, phoneNumber, login, accountId, walletId, id, state);
    }

    public User getUser() {
        return data;
    }
}

package org.stellar.sdk.responses;


import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Map;

public enum UserState {
    NOT_APPROVED, APPROVED, REJECTED, REMOVED;

    private static final Map<String, UserState> statesMap;

    static {
        statesMap = new Hashtable<String, UserState>();
        statesMap.put("NOT_APPROVED", NOT_APPROVED);
        statesMap.put("APPROVED", APPROVED);
        statesMap.put("REJECTED", REJECTED);
        statesMap.put("REMOVED", REMOVED);
    }

    public static UserState toEnum(final String rawState) {
        if (rawState == null)
            return NOT_APPROVED;
        final String state = rawState.toUpperCase();
        UserState result = statesMap.get(state);
        return result == null ? NOT_APPROVED : result;
    }
}

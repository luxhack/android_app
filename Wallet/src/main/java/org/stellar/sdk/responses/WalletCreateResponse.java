package org.stellar.sdk.responses;

import com.google.gson.annotations.SerializedName;

public class WalletCreateResponse {
    @SerializedName("status")
    private String status;
    @SerializedName("field")
    private String field;
    @SerializedName("code")
    private String code;

    public WalletCreateResponse(String status, String field, String code) {
        this.status = status;
        this.field = field;
        this.code = code;
    }

    public boolean isSuccess(){
        return status.equals("success");
    }

    public String getField() {
        return field;
    }

    public String getCode() {
        return code;
    }
}

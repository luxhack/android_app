package org.stellar.sdk.responses.operations;

import com.google.gson.annotations.SerializedName;

import org.stellar.sdk.Asset;
import org.stellar.sdk.AssetTypeNative;
import org.stellar.sdk.KeyPair;

/**
 * Represents PathPayment operation response.
 * @see <a href="https://www.stellar.org/developers/horizon/reference/resources/operation.html" target="_blank">Operation documentation</a>
 * @see org.stellar.sdk.requests.OperationsRequestBuilder
 * @see org.stellar.sdk.Server#operations()
 */
public class PathPaymentOperationResponse extends PaymentOperationResponse {
  @SerializedName("source_amount")
  protected final String sourceAmount;
    @SerializedName("source_max")
    protected final String sourceMax;

  @SerializedName("source_asset_type")
  protected final String sourceAssetType;
  @SerializedName("source_asset_code")
  protected final String sourceAssetCode;
  @SerializedName("source_asset_issuer")
  protected final String sourceAssetIssuer;

  public PathPaymentOperationResponse(String amount, String assetType, String assetCode, String assetIssuer,
                                      KeyPair from, KeyPair to, String sourceAmount, String sourceMax, String sourceAssetType,
                                      String sourceAssetCode, String sourceAssetIssuer) {
    super(amount, assetType, assetCode, assetIssuer, from, to);
    this.sourceAmount = sourceAmount;
      this.sourceMax = sourceMax;
    this.sourceAssetType = sourceAssetType;
    this.sourceAssetCode = sourceAssetCode;
    this.sourceAssetIssuer = sourceAssetIssuer;
  }

    public String getSourceMax() {
        return sourceMax;
    }

    public String getSourceAmount() {
    return sourceAmount;
  }

    public String getReceivedAmount() { return super.getAmount();}

    public Asset getReceivedAsset() {return super.getAsset();}

    @Override
    public String getAmount() {
        return this.getSourceAmount();
    }

    public Asset getSourceAsset() {
    if (sourceAssetType.equals("native")) {
      return new AssetTypeNative();
    } else {
      KeyPair issuer = KeyPair.fromAccountId(sourceAssetIssuer);
      return Asset.createNonNativeAsset(sourceAssetCode, issuer);
    }
  }

    @Override
    public Asset getAsset() {
        return this.getSourceAsset();
    }

    @Override
    public String toString() {
        return "PathPaymentOperationResponse{" +
                "sourceAmount='" + sourceAmount + '\'' +
                ", sourceAssetType='" + sourceAssetType + '\'' +
                ", sourceAssetCode='" + sourceAssetCode + '\'' +
                ", sourceAssetIssuer='" + sourceAssetIssuer + '\'' +
                "} " + super.toString();
    }
}

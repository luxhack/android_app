package org.stellar.sdk.wallet;

import com.google.gson.annotations.SerializedName;

import org.stellar.sdk.responses.GsonSingleton;

class KeychainData {
    @SerializedName("IV")
    private final String rawIV;
    @SerializedName("cipherText")
    private final String rawCipherText;

    public KeychainData(String rawIV, String rawCipherText) {
        this.rawIV = rawIV;
        this.rawCipherText = rawCipherText;
    }

    public static KeychainData getFromRaw(final Blob rawData) {
        final String decoded = Wallet.BlobToString(rawData);
        return getFromJson(decoded);
    }

    private static KeychainData getFromJson(final String rawJson) {
        return GsonSingleton.getInstance().fromJson(rawJson, KeychainData.class);
    }

    public Blob getIV() {
        return Wallet.Decode(this.rawIV);
    }

    public Blob getCipherText() {
        return Wallet.Decode(this.rawCipherText);
    }
}

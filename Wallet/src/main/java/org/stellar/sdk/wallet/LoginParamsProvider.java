package org.stellar.sdk.wallet;


import com.google.gson.reflect.TypeToken;
import org.stellar.sdk.HttpClient;
import org.stellar.sdk.federation.ServerErrorException;

import java.io.UnsupportedEncodingException;
import java.net.URI;

class LoginParamsProvider {


    public static LoginParamsResponse getLoginParams(final String login, final URI walletUri) {
        try {
            return tryGetLoginParams(login, walletUri);
        } catch (UnsupportedEncodingException ex) {
            throw new ServerErrorException();
        }
    }

    private static LoginParamsResponse tryGetLoginParams(final String login, final URI walletUri) throws UnsupportedEncodingException {
        final URI showLoginParamsUri = URI.create(walletUri.toString() + "/wallets/show_login_params");
        final LoginParamsRequest loginParamsRequest = new LoginParamsRequest(login);
        return HttpClient.executeJsonPostRequest(showLoginParamsUri, loginParamsRequest,
                TypeToken.get(LoginParamsResponse.class));
    }
}

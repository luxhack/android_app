package org.stellar.sdk.wallet;


import com.google.gson.annotations.SerializedName;

class LoginParamsRequest {
    @SerializedName("username")
    private final String username;

    public LoginParamsRequest(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    @Override
    public String toString() {
        return "LoginParamsRequest{" +
                "username='" + username + '\'' +
                '}';
    }
}

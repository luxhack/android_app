package org.stellar.sdk.wallet;


import android.provider.Settings;
import android.util.Log;

import com.google.gson.annotations.SerializedName;

class LoginParamsResponse {
    @SerializedName("username")
    private final String username;
    @SerializedName("salt")
    private final String rawSalt;
    @SerializedName("kdfParams")
    private final String kdfParams;
    @SerializedName("totpRequired")
    private final boolean totpRequired;

    public LoginParamsResponse(String username, String rawSalt, String kdfParams, boolean totpRequired) {
        this.username = username;
        this.rawSalt = rawSalt;
        this.kdfParams = kdfParams;
        this.totpRequired = totpRequired;
    }

    public String getUsername() {
        return username;
    }

    public String getRawSalt() {
        return rawSalt;
    }

    public String getKdfParams() {
        return kdfParams;
    }

    public boolean isTotpRequired() {
        return totpRequired;
    }

    public Blob getSalt() {
        if (this.rawSalt == null)
            return null;
        Log.d("getSalt", "getSalt: " + rawSalt);
        return Wallet.Decode(this.rawSalt);
    }

    @Override
    public String toString() {
        return "LoginParamsResponse{" +
                "username='" + username + '\'' +
                ", rawSalt='" + rawSalt + '\'' +
                ", kdfParams='" + kdfParams + '\'' +
                ", totpRequired=" + totpRequired +
                '}';
    }
}

package org.stellar.sdk.wallet;

import com.google.gson.annotations.SerializedName;

class WalletData {

    @SerializedName("lockVersion")
    private final int lockVersion;
    @SerializedName("mainData")
    private final String mainData;
    @SerializedName("keychainData")
    private final String rawKeychainData;

    public WalletData(int lockVersion, String mainData, String rawKeychainData) {
        this.lockVersion = lockVersion;
        this.mainData = mainData;
        this.rawKeychainData = rawKeychainData;
    }

    public int getLockVersion() {
        return lockVersion;
    }

    public String getMainData() {
        return mainData;
    }

    public Blob getIV() {
        final KeychainData keychainData = this.getKeychainData();
        return keychainData == null ? null : keychainData.getIV();
    }

    public Blob getCipherText() {
        final KeychainData keychainData = this.getKeychainData();
        return keychainData == null ? null : keychainData.getCipherText();
    }

    private KeychainData getKeychainData() {
        if (this.rawKeychainData == null)
            return null;
        return KeychainData.getFromRaw(Wallet.Decode(this.rawKeychainData));
    }

    @Override
    public String toString() {
        return "WalletData{" +
                "lockVersion=" + lockVersion +
                ", mainData='" + mainData + '\'' +
                ", rawKeychainData='" + rawKeychainData + '}';
    }
}

package org.stellar.sdk.wallet;


import android.util.Log;

import com.google.gson.reflect.TypeToken;
import org.stellar.sdk.HttpClient;

import java.net.URI;

class WalletDataProvider {

    public static WalletData getWalletData(final String login, final String walletId, final URI walletUri) {
        final URI showLoginParamsUri = URI.create(walletUri.toString() + "/wallets/show");
        final WalletDataRequest request = new WalletDataRequest(login, walletId);
        //Log.d("getWalletData", "requestURI: " + request.)
        return HttpClient.executeJsonPostRequest(showLoginParamsUri, request,
                TypeToken.get(WalletData.class));
    }
}

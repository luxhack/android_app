package org.stellar.sdk.wallet;


import com.google.gson.annotations.SerializedName;

class WalletDataRequest {
    @SerializedName("username")
    private final String username;
    @SerializedName("walletId")
    private final String walletId;

    public WalletDataRequest(String username, String walletId) {
        this.username = username;
        this.walletId = walletId;
    }

    public String getUsername() {
        return username;
    }

    public String getWalletId() {
        return walletId;
    }

    @Override
    public String toString() {
        return "WalletDataRequest{" +
                "username='" + username + '\'' +
                ", walletId='" + walletId + '\'' +
                '}';
    }
}

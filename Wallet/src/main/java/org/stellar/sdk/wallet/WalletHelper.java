package org.stellar.sdk.wallet;


import org.stellar.sdk.Config;

class WalletHelper {
    public static String formatLogin(final String login) {
        final String[] tokens = login.split("@");
        return tokens.length == 1 ? login + "@" + Config.DEFAULT_DOMAIN : login;
    }

    public static String base64(final byte[] data) {
        final String strData = new String(data);
        final Blob blobData = Wallet.StringToBlob(strData);
        return Wallet.Encode(blobData);
    }
}

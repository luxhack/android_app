package org.stellar.sdk.wallet;

import org.apache.commons.codec.DecoderException;
import org.stellar.sdk.Config;
import org.stellar.sdk.KeyPair;
import org.stellar.sdk.federation.NotFoundException;

import java.io.UnsupportedEncodingException;
import java.net.URI;

public class WalletManager {

    public static KeyPair getKeyPair(final String login, final String password, final String walletUri) {
        try {
            return getKeyPair(login, password, URI.create(walletUri));
        } catch (Exception ex) {
            throw new NotFoundException(ex);
        }
    }

    /**
     * Gets encrypted wallet from server defined by <code>walletUri</code> and tries to decrypt it.
     * Returns null if fails.
     * @param login - user's login in format "bob@cobra.com" or "bob". In last case default domain is used.
     * @param password - user's password
     * @param walletUri - url to wallet storage
     * @return key pair or null if failed to decrypt
     */
    public static KeyPair getKeyPair(String login, final String password, final URI walletUri) {
        //login = WalletHelper.formatLogin(login);
        final Blob salt = getSalt(login, walletUri);
        final String walletId = getWalletId(login, password, salt, walletUri);
        final WalletData walletData = WalletDataProvider.getWalletData(login, walletId, walletUri);
        Blob key = Wallet.DeriveKey(login, password, salt);
        final String pk = getPrivateKey(key, walletData);
        return KeyPair.fromSecretSeed(pk);
    }

    protected static String getWalletId(String login, final String password, Blob salt, final URI walletUri) {
        final Blob rawWalletId = Wallet.DeriveWalletId(login, password, salt);
        return Wallet.Encode(rawWalletId);
    }

    protected static String getPrivateKey(final Blob key,
                                   final WalletData walletData) {

        Blob iv = walletData.getIV();
        Blob cipherText = walletData.getCipherText();
        Blob rawWalletSecret = Wallet.Decrypt(key, iv, cipherText);
        final WalletSecret secret = WalletSecret.fromBlob(rawWalletSecret);
        return secret.getSeed();
    }


    private static Blob getSalt(final String formattedLogin, final URI walletUri) {
        final LoginParamsResponse loginParams = LoginParamsProvider.getLoginParams(formattedLogin, walletUri);
        if (loginParams == null)
            return null;
        return loginParams.getSalt();
    }

}

package org.stellar.sdk.wallet;

import com.google.gson.annotations.SerializedName;
import org.stellar.sdk.KeyPair;
import org.stellar.sdk.responses.GsonSingleton;

public class WalletSaveRequest {
    @SerializedName("username")
    private final String username;
    @SerializedName("walletId")
    private final String walletId;
    @SerializedName("salt")
    private final String salt;
    @SerializedName("publicKey")
    private final String publicKey;
    @SerializedName("mainData")
    private final String mainData;
    @SerializedName("mainDataHash")
    private final String mainDataHash;
    @SerializedName("keychainData")
    private final String keychainData;
    @SerializedName("keychainDataHash")
    private final String keychainHash;
    @SerializedName("kdfParams")
    private final String kdfParams;
    @SerializedName("usernameProof")
    private final String usernameProof;

    public WalletSaveRequest(String username, String walletId, String salt, String publicKey, String mainData,
                             String mainDataHash, String keychainData, String keychainHash, String kdfParams,
                             String usernameProof) {
        this.username = username;
        this.walletId = walletId;
        this.salt = salt;
        this.publicKey = publicKey;
        this.mainData = mainData;
        this.mainDataHash = mainDataHash;
        this.keychainData = keychainData;
        this.keychainHash = keychainHash;
        this.kdfParams = kdfParams;
        this.usernameProof = usernameProof;
    }

    public String getUsername() {
        return username;
    }

    public String getWalletId() {
        return walletId;
    }

    public String getSalt() {
        return salt;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public String getMainData() {
        return mainData;
    }


    public String getKeychainData() {
        return keychainData;
    }

    public String getKdfParams() {
        return kdfParams;
    }

    public String getMainDataHash() {
        return mainDataHash;
    }

    public String getKeychainHash() {
        return keychainHash;
    }

    public String getUsernameProof() {
        return usernameProof;
    }
}

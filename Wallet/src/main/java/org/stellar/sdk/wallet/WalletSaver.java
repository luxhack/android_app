package org.stellar.sdk.wallet;

import com.google.gson.annotations.SerializedName;

import org.apache.commons.codec.binary.Base64;
import org.stellar.sdk.KeyPair;
import org.stellar.sdk.responses.GsonSingleton;

import java.net.URI;

public class WalletSaver {

    public static WalletSaveRequest createEncryptedWallet(final String login, final String password, final KeyPair keys,
                                         final URI storageUri) {
        final String userNameData = WalletHelper.formatLogin(login);
        final String[] userNameTokens = userNameData.split("@");
        if (userNameTokens.length != 2) {
            throw new IllegalArgumentException("Login has invalid format!");
        }
        final String username = userNameTokens[0];
        final String domain = userNameTokens[1];
        final String encryptedWallet = createEncryptedWallet(username, domain, password, keys, storageUri);
        return GsonSingleton.getInstance().fromJson(encryptedWallet, WalletSaveRequest.class);
    }

    private static String createEncryptedWallet(final String username, final String domain,
                                          final String password, final KeyPair keys,
                                          final URI storageUri) {
        final String pubKeyBase64 = new String(Base64.encodeBase64(keys.getPublicKey()));
        return Wallet.CreateEncryptedWallet(username, domain, password, keys.getSecretSeed(),
                pubKeyBase64, storageUri.toString(), storageUri.getPort(),
                false, true);
    }
}

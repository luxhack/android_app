package org.stellar.sdk.wallet;

import com.google.gson.annotations.SerializedName;

import org.stellar.sdk.responses.GsonSingleton;

class WalletSecret {
    @SerializedName("seed")
    private final String seed;

    public WalletSecret(String seed) {
        this.seed = seed;
    }

    public static WalletSecret fromBlob(final Blob blob) {
        final String rawJson = Wallet.BlobToString(blob);
        return fromRawJson(rawJson);
    }

    private static WalletSecret fromRawJson(final String rawJson) {
        return GsonSingleton.getInstance().fromJson(rawJson, WalletSecret.class);
    }

    public String getSeed() {
        return this.seed;
    }
}

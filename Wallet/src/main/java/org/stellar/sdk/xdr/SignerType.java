// Automatically generated by xdrgen 
// DO NOT EDIT or your changes may be overwritten

package org.stellar.sdk.xdr;


import java.io.IOException;

// === xdr source ============================================================

//  enum SignerType
//  {
//      SIGNER_GENERAL = 0,
//      SIGNER_ADMIN = 1,
//      SIGNER_EMISSION = 2
//  };

//  ===========================================================================
public enum SignerType  {
  SIGNER_GENERAL(0),
  SIGNER_ADMIN(1),
  SIGNER_EMISSION(2),
  ;
  private int mValue;

  SignerType(int value) {
      mValue = value;
  }

  public int getValue() {
      return mValue;
  }

  static SignerType decode(XdrDataInputStream stream) throws IOException {
    int value = stream.readInt();
    switch (value) {
      case 0: return SIGNER_GENERAL;
      case 1: return SIGNER_ADMIN;
      case 2: return SIGNER_EMISSION;
      default:
        throw new RuntimeException("Unknown enum value: " + value);
    }
  }

  static void encode(XdrDataOutputStream stream, SignerType value) throws IOException {
    stream.writeInt(value.getValue());
  }
}

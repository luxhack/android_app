package org.stellar.sdk.federation;

import junit.framework.TestCase;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.HttpVersion;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.message.BasicStatusLine;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

public class FederationTest extends TestCase {
  @Mock
  private HttpClient mockClient;
  @Mock
  private HttpResponse mockResponse;
  @Mock
  private HttpEntity mockEntity;

  private final StatusLine httpOK = new BasicStatusLine(HttpVersion.HTTP_1_1, HttpStatus.SC_OK, "OK");
  private final String successResponse =
          "{\"success\":true,\"data\":{\"accountId\":\"gnzuRv2YAuHPp1eRL81Aace5Dq9PVNMLsQ\"}}";
  private final StatusLine httpNotFound = new BasicStatusLine(HttpVersion.HTTP_1_1, HttpStatus.SC_NOT_FOUND, "NOT_FOUND");
  private final String notFoundResponse =
          "{\"success\":false,\"error\":\"User not found\"}";

  private final String reverseSuccessResponse = "{\"success\":true,\"data\":{\"username\":\"dmitriy\",\"domain\":\"cobra.com\"}}";

  public void setUp() throws URISyntaxException, IOException {
    MockitoAnnotations.initMocks(this);
    org.stellar.sdk.HttpClient.setHttpClient(mockClient);
    when(mockResponse.getEntity()).thenReturn(mockEntity);
    when(mockClient.execute((HttpRequestBase) any())).thenReturn(mockResponse);
  }

  @Test
  public void testResolveFullNameSuccess() throws IOException {
    InputStream federationResponse = new ByteArrayInputStream(successResponse.getBytes(StandardCharsets.UTF_8));

    when(mockResponse.getStatusLine()).thenReturn(httpOK, httpOK);
    when(mockEntity.getContent()).thenReturn(federationResponse);

    FederationResponse fullNameResp = Federation.resolve("dmitriy@cobra.com");
    successCheck(fullNameResp);
  }

  @Test
  public void testResolveNameSuccess() throws IOException {
    InputStream federationResponse = new ByteArrayInputStream(successResponse.getBytes(StandardCharsets.UTF_8));

    when(mockResponse.getStatusLine()).thenReturn(httpOK, httpOK);
    when(mockEntity.getContent()).thenReturn(federationResponse);

    FederationResponse nameResp = Federation.resolve("dmitriy");
    successCheck(nameResp);
  }

  @Test
  public void testResolveAddressSuccess() throws IOException {
    InputStream federationResponse = new ByteArrayInputStream(reverseSuccessResponse.getBytes(StandardCharsets.UTF_8));
    InputStream jsonResponse = new ByteArrayInputStream(notFoundResponse.getBytes(StandardCharsets.UTF_8));
    when(mockResponse.getStatusLine()).thenReturn(httpNotFound, httpOK);
    when(mockEntity.getContent()).thenReturn(federationResponse);

    FederationResponse addressResp = Federation.resolve("gnzuRv2YAuHPp1eRL81Aace5Dq9PVNMLsQ");
    successCheck(addressResp);
  }

  private void successCheck(final FederationResponse response) {
    assertEquals(response.getUsername(), "dmitriy");
    assertEquals(response.getAccountId(), "gnzuRv2YAuHPp1eRL81Aace5Dq9PVNMLsQ");
    assertEquals(response.getDomain(), "cobra.com");
    assertNull(response.getError());
    assertTrue(response.isSuccess());
  }

  @Test
  public void testMalformedAddress() {
    try {
      FederationResponse response = Federation.resolve("bob@stellar.org@test");
      fail("Expected exception");
    } catch (MalformedAddressException e) {
      //
    }
  }
}

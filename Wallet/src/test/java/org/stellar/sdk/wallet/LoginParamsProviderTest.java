package org.stellar.sdk.wallet;

import junit.framework.TestCase;
import org.apache.http.*;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.message.BasicStatusLine;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.stellar.sdk.federation.NotFoundException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

public class LoginParamsProviderTest extends TestCase {
  @Mock
  private HttpClient mockClient;
  @Mock
  private HttpResponse mockResponse;
  @Mock
  private HttpEntity mockEntity;

  private final StatusLine httpOK = new BasicStatusLine(HttpVersion.HTTP_1_1, HttpStatus.SC_OK, "OK");
  private final String successResponse =
          "{\"username\":\"dmitriy@cobraapi.azurewebsites.net\",\"salt\":\"fRk7wcFBmvQHR50hNeYW9w==\",\"kdfParams\":\"{\\\"algorithm\\\":\\\"scrypt\\\",\\\"bits\\\":256,\\\"n\\\":4096,\\\"r\\\":8,\\\"p\\\":1}\",\"totpRequired\":false}";

  private final StatusLine httpNotFound = new BasicStatusLine(HttpVersion.HTTP_1_1, HttpStatus.SC_NOT_FOUND, "NOT_FOUND");
  private final String notFoundResponse = "{\"status\":\"fail\",\"code\":\"not_found\"}";

  public void setUp() throws URISyntaxException, IOException {
    MockitoAnnotations.initMocks(this);
    org.stellar.sdk.HttpClient.setHttpClient(mockClient);
    when(mockResponse.getEntity()).thenReturn(mockEntity);
    when(mockClient.execute((HttpRequestBase) any())).thenReturn(mockResponse);
  }

  @Test
  public void testResolveSuccess() throws IOException, URISyntaxException {
    InputStream showLoginParamsResponse = new ByteArrayInputStream(successResponse.getBytes(StandardCharsets.UTF_8));

    when(mockResponse.getStatusLine()).thenReturn(httpOK, httpOK);
    when(mockEntity.getContent()).thenReturn(showLoginParamsResponse);

    LoginParamsResponse response = LoginParamsProvider.getLoginParams("dmitriy@cobraapi.azurewebsites.net", URI.create("http://test.com"));
    assertEquals(response.getUsername(), "dmitriy@cobraapi.azurewebsites.net");
    assertEquals(response.getRawSalt(), "fRk7wcFBmvQHR50hNeYW9w==");
    assertEquals(response.getKdfParams(), "{\"algorithm\":\"scrypt\",\"bits\":256,\"n\":4096,\"r\":8,\"p\":1}");
    assertEquals(response.isTotpRequired(), false);
  }

  @Test
  public void testLoginParamsNotFound() throws IOException, URISyntaxException {
    InputStream jsonResponse = new ByteArrayInputStream(notFoundResponse.getBytes(StandardCharsets.UTF_8));
    when(mockResponse.getStatusLine()).thenReturn(httpNotFound);
    when(mockEntity.getContent()).thenReturn(jsonResponse);

    try {
      LoginParamsResponse response = LoginParamsProvider.getLoginParams("dmitriy@cobraapi.azurewebsites.net", URI.create("http://test.com"));
      fail("Expected exception");
    } catch (NotFoundException e) {}
  }
}

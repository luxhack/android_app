package com.distributedlab.hackathonluxembourg;

import android.app.Application;
import android.content.Context;

/**
 * Created by yaroslav on 5/8/17.
 */

public class App extends Application {
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();

        context = this;
    }

    public static Context getContext() {
        return context;
    }
}

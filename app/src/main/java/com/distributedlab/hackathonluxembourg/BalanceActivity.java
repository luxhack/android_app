package com.distributedlab.hackathonluxembourg;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.distributedlab.hackathonluxembourg.logic.ApiHelper;
import com.distributedlab.hackathonluxembourg.logic.RefreshDataTask;
import com.distributedlab.hackathonluxembourg.logic.StorageHelper;
import com.distributedlab.hackathonluxembourg.models.Balance;
import com.distributedlab.hackathonluxembourg.models.Transaction;
import com.distributedlab.hackathonluxembourg.models.User;
import com.distributedlab.hackathonluxembourg.ui.adapters.HistoryAdapter;
import com.distributedlab.hackathonluxembourg.ui.adapters.TxHistoryItemAnimator;
import com.distributedlab.hackathonluxembourg.util.AmountFormatter;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class BalanceActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private HistoryAdapter historyAdapter;

    private String currentAsset = "EUR";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_balance);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        initHistory();
        initActions();
        initAccountData(navigationView.getHeaderView(0));
    }

    private void initAccountData(View v) {
        User user = StorageHelper.getInstance().getUser();
        ((TextView)v.findViewById(R.id.user_name)).setText(user.getName());
        ((TextView)v.findViewById(R.id.user_email)).setText(user.getEmail());
        ((ImageView)v.findViewById(R.id.user_photo)).setImageDrawable(getResources().getDrawable(user.getPicture()));
    }

    private void initActions() {
        final FloatingActionMenu menu = (FloatingActionMenu) findViewById(R.id.actions_menu);
        menu.setClosedOnTouchOutside(true);

        FloatingActionButton exchangeBtn = (FloatingActionButton) findViewById(R.id.sell_buy_fab);
        exchangeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                menu.close(false);
                onExchangeOpen();
            }
        });
    }

    private void initHistory() {
        RecyclerView history = (RecyclerView) findViewById(R.id.history);
        history.setLayoutManager(new LinearLayoutManager(this));

        historyAdapter = new HistoryAdapter();
        history.setAdapter(historyAdapter);

        history.setItemAnimator(new TxHistoryItemAnimator(this));
    }

    private void redraw() {
        Balance balance = StorageHelper.getInstance().getBalance(currentAsset);
        ((CollapsingToolbarLayout)findViewById(R.id.collapsing_toolbar))
                .setTitle(AmountFormatter.formatWithCurrencySign(balance.getAmount(), balance.getAsset()));

        historyAdapter.setData(StorageHelper.getInstance().getTransactions(currentAsset));
    }

    @Override
    protected void onResume() {
        super.onResume();

        redraw();

        new RefreshDataTask(new Runnable() {
            @Override
            public void run() {
                redraw();
            }
        }).execute();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id) {
            case R.id.nav_usd:
                currentAsset = "EUR";
                redraw();
                break;
            case R.id.nav_aaa:
                currentAsset = "AAA";
                redraw();
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void onExchangeOpen() {
        Intent intent = new Intent(this, OrderbookActivity.class);
        intent.putExtra(OrderbookActivity.CUR_CURRENCY, currentAsset);
        startActivity(intent);
    }
}

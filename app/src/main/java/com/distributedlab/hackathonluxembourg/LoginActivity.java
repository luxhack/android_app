package com.distributedlab.hackathonluxembourg;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.TransitionDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.distributedlab.hackathonluxembourg.logic.ApiHelper;
import com.distributedlab.hackathonluxembourg.models.User;
import com.distributedlab.hackathonluxembourg.util.Strings;

import org.stellar.sdk.KeyPair;

/**
 * Created by yaroslav on 5/8/17.
 */

public class LoginActivity extends AppCompatActivity {
    private EditText passwordEditText;
    private EditText loginEditText;

    private ApiHelper apiHelper;

    // Animation components
    private View animLogo, background, logo, appName, pass, login, buttons, forgotPassword;
    private final float SCALE_EXPAND = 2.2f, SCALE_CONSTRICT = 0.8f,
            ALPHA_HIDDEN = 0.0f, ALPHA_VISIBLE = 1.0f;
    private final int ANIM_DURATION = 500;

    public static final String LOGIN_EXTRA = "sendLoginQuery";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);
        initViews();

        apiHelper = ApiHelper.getInstance();

        launchAnimation();
    }

    // region Init views.
    private void initViews() {
        initAnimationViews();
        loginEditText = (EditText) findViewById(R.id.login_edit_text);
        initPasswordEditText();
        initLoginButton();
        initRegisterButton();
    }

    private void initAnimationViews() {
        animLogo = findViewById(R.id.animate_logo);
        background = findViewById(R.id.background);
        appName = findViewById(R.id.app_name);
        logo = findViewById(R.id.logo);
        pass = findViewById(R.id.passHolder);
        login = findViewById(R.id.loginHolder);
        buttons = findViewById(R.id.buttons);
        forgotPassword = findViewById(R.id.forgot_password_btn);
    }

    private void initPasswordEditText() {
        passwordEditText = (EditText) findViewById(R.id.password_editText);
        passwordEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE) {
                    onLoginPressed();
                }
                return false;
            }
        });
    }

    private void initLoginButton() {
        Button loginButton = (Button) findViewById(R.id.login_btn);
        if (loginButton != null) {
            loginButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onLoginPressed();
                }
            });
        }
    }

    private void initRegisterButton() {
        Button registerBtn = (Button) findViewById(R.id.registration_btn);
        if (registerBtn != null) {
            registerBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onRegisterPressed();
                }
            });
        }
    }
    // endregion Init views.

    private void launchAnimation() {
        int currentAnimTime = 1000;
        final Handler handler = new Handler();

        background.setVisibility(View.VISIBLE);
        hideLoginViews();

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startLogoAnimationTransition();
            }
        }, currentAnimTime);

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                showLoginViews();
            }
        }, currentAnimTime += ANIM_DURATION / 2);
    }

    private void showLoginAnimation(Runnable callback) {
        int currentAnimTime = 0;
        final Handler handler = new Handler();

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                hideLoginViews();

                animLogo.animate().scaleX(SCALE_EXPAND).scaleY(SCALE_EXPAND)
                        .alpha(ALPHA_HIDDEN).setDuration(ANIM_DURATION);
            }
        }, currentAnimTime += ANIM_DURATION * 1.25);


        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                logo.animate().alpha(ALPHA_VISIBLE)
                        .scaleY(SCALE_CONSTRICT).scaleX(SCALE_CONSTRICT)
                        .setDuration(ANIM_DURATION / 2);

                showLoginViews();
            }
        }, currentAnimTime += ANIM_DURATION);

        handler.postDelayed(callback, currentAnimTime);
    }

    private void startLogoAnimationTransition() {
        animLogo.animate().scaleX(SCALE_CONSTRICT).scaleY(SCALE_CONSTRICT)
                .setDuration(ANIM_DURATION);
        TransitionDrawable transition = (TransitionDrawable) background.getBackground();
        transition.startTransition(ANIM_DURATION * 2);
    }

    private void hideLoginViews() {
        logo.setAlpha(ALPHA_HIDDEN);
        appName.setAlpha(ALPHA_HIDDEN);
        pass.setAlpha(ALPHA_HIDDEN);
        login.setAlpha(ALPHA_HIDDEN);
        buttons.setAlpha(ALPHA_HIDDEN);
        forgotPassword.setAlpha(ALPHA_HIDDEN);
    }

    private void showLoginViews() {
        appName.animate().alpha(ALPHA_VISIBLE).setDuration(ANIM_DURATION);
        pass.animate().alpha(ALPHA_VISIBLE).setDuration(ANIM_DURATION);
        buttons.animate().alpha(ALPHA_VISIBLE).setDuration(ANIM_DURATION);
        forgotPassword.animate().alpha(ALPHA_VISIBLE).setDuration(ANIM_DURATION);
        login.animate().alpha(ALPHA_VISIBLE).setDuration(ANIM_DURATION);
    }

    private void onLoginPressed() {
        hideKeyboard();
        String login = loginEditText.getText().toString();
        String password = passwordEditText.getText().toString();

        User user = apiHelper.login(login, password);

        if (user != null) {
            showLoginAnimation(new Runnable() {
                @Override
                public void run() {
                    startActivity(new Intent(LoginActivity.this, BalanceActivity.class));
                    finish();
                }
            });
        } else {
            View parentLayout = findViewById(android.R.id.content);
            Snackbar.make(parentLayout, R.string.error_login, Snackbar.LENGTH_LONG).show();
        }
    }

    private void onRegisterPressed() {
        startActivity(new Intent(this, KycActivity.class));
    }

    private void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}

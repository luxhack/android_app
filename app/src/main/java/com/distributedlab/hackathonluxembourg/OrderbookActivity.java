package com.distributedlab.hackathonluxembourg;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.design.widget.TabLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.EditText;
import android.widget.TextView;

import com.distributedlab.hackathonluxembourg.logic.CreateOrderTask;
import com.distributedlab.hackathonluxembourg.logic.StorageHelper;
import com.distributedlab.hackathonluxembourg.models.Order;
import com.distributedlab.hackathonluxembourg.ui.adapters.OrderbookAdapter;
import com.distributedlab.hackathonluxembourg.util.Strings;
import com.github.clans.fab.FloatingActionButton;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class OrderbookActivity extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    private View dialogContent;

    private String currentCurrency = "EUR";

    public static final String CUR_CURRENCY = "cur_currency";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orderbook);

        currentCurrency = getIntent().getStringExtra(CUR_CURRENCY);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(String.format("%s orderbook", currentCurrency));
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        initFab();
    }

    private void initFab() {
        FloatingActionButton btn = (FloatingActionButton)findViewById(R.id.create_offer);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCreateOfferDialog();
            }
        });
    }

    private void showCreateOfferDialog() {
        dialogContent = LayoutInflater.from(this).inflate(R.layout.dialog_create_offer, null, false);
        final EditText amount = (EditText) dialogContent.findViewById(R.id.amount);
        final EditText price = (EditText) dialogContent.findViewById(R.id.price);

        final AlertDialog dialog = new AlertDialog.Builder(this)
                .setCancelable(false)
                .setTitle(mViewPager.getCurrentItem() == 0 ? R.string.buy : R.string.sell)
                .setNegativeButton(R.string.cancel, null)
                .setPositiveButton(R.string.create, null)
                .setView(dialogContent)
                .create();

        dialog.show();

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (createOffer(amount.getText().toString(), price.getText().toString()))
                    dialog.dismiss();
            }
        });
    }

    private boolean createOffer(String amount, String price) {
        if (Strings.isNullEmptyOrWhitespaces(amount) || Strings.isNullEmptyOrWhitespaces(price)) {
            showError("Fields can’t be empty", dialogContent);
            return false;
        }

        BigDecimal am = new BigDecimal(amount);
        BigDecimal prc = new BigDecimal(price);

        String sourceAsset;
        String distAsset;
        BigDecimal sourceAmount;
        BigDecimal distAmount;

        if (currentCurrency.equals("EUR")) {
            if (mViewPager.getCurrentItem() == 1) {
                sourceAsset = "EUR";
                distAsset = "AAA";
                sourceAmount = am;
                distAmount = am.multiply(prc);
            } else {
                sourceAsset = "AAA";
                distAsset = "EUR";
                sourceAmount = am.divide(prc);
                distAmount = am;
            }
        } else {
            if (mViewPager.getCurrentItem() == 1) {
                sourceAsset = "AAA";
                distAsset = "EUR";
                sourceAmount = am;
                distAmount = am.multiply(prc);
            } else {
                sourceAsset = "EUR";
                distAsset = "AAA";
                sourceAmount = am.divide(prc);
                distAmount = am;
            }
        }
        Order order = new Order(sourceAsset, distAsset, sourceAmount, distAmount);

        return createOrder(order);
    }

    private boolean createOrder(Order order) {
        if (order.getSourceAmount().compareTo(StorageHelper.getInstance().getBalance(order.getSourceAsset()).getAmount()) == 1) {
            showError("Not enough money", dialogContent);
            return false;
        }

        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("Submiting...");
        dialog.show();

        new CreateOrderTask(new CreateOrderTask.OnOrderCreated() {
            @Override
            public void run(Boolean isSuccess, Order order) {
                dialog.dismiss();
                Snackbar.make(findViewById(android.R.id.content), isSuccess ? "Success" : "Error", Snackbar.LENGTH_LONG);
            }
        }).execute(order);

        return true;
    }

    private void showError(String mes, View parentView) {
        Snackbar.make(parentView, mes, Snackbar.LENGTH_LONG).show();
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        private List<Order> orders;
        private View rootView;
        private boolean isSell;
        private String currentCurrency;

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(List<Order> orders, boolean isSell, String curentCurrency) {
            PlaceholderFragment fragment = new PlaceholderFragment();

            fragment.orders = new ArrayList<>(orders);
            fragment.isSell = isSell;
            fragment.currentCurrency = curentCurrency;

            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            rootView = inflater.inflate(R.layout.fragment_orderbook, container, false);

            initOrderList(rootView);

            return rootView;
        }

        private void initOrderList(View v) {
            RecyclerView ordersList = (RecyclerView) v.findViewById(R.id.orders);
            ordersList.setLayoutManager(new LinearLayoutManager(getActivity()));

            OrderbookAdapter adapter = new OrderbookAdapter(filterOrder(), new OrderbookAdapter.OnOrderbookClick() {
                @Override
                public void onClick(Order order) {

                }
            });
            ordersList.setAdapter(adapter);
        }

        private List<Order> filterOrder() {
            List<Order> filtered = new ArrayList<>();

            if (isSell) {
                for (Order order : orders) {
                    if (order.getSourceAsset().equals(currentCurrency))
                        filtered.add(order);
                }
            } else {
                for (Order order : orders) {
                    if (order.getDistAsset().equals(currentCurrency))
                        filtered.add(order);
                }
            }

            return filtered;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return PlaceholderFragment.newInstance(StorageHelper.getInstance().getOrders(), position == 1, currentCurrency);
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getString(R.string.buy);
                case 1:
                    return getString(R.string.sell);
            }
            return null;
        }
    }
}

package com.distributedlab.hackathonluxembourg.logic;

import android.support.annotation.Nullable;

import com.distributedlab.hackathonluxembourg.R;
import com.distributedlab.hackathonluxembourg.models.Balance;
import com.distributedlab.hackathonluxembourg.models.Order;
import com.distributedlab.hackathonluxembourg.models.Transaction;
import com.distributedlab.hackathonluxembourg.models.User;
import com.distributedlab.hackathonluxembourg.util.Strings;

import org.stellar.sdk.Account;
import org.stellar.sdk.Asset;
import org.stellar.sdk.CreatePassiveOfferOperation;
import org.stellar.sdk.KeyPair;
import org.stellar.sdk.ManageOfferOperation;
import org.stellar.sdk.Memo;
import org.stellar.sdk.MemoHash;
import org.stellar.sdk.MemoId;
import org.stellar.sdk.MemoText;
import org.stellar.sdk.Price;
import org.stellar.sdk.Server;
import org.stellar.sdk.responses.AccountResponse;
import org.stellar.sdk.responses.OrderBook.OfferData;
import org.stellar.sdk.responses.OrderBookResponse;
import org.stellar.sdk.responses.SubmitTransactionResponse;
import org.stellar.sdk.responses.operations.PaymentOperationResponse;
import org.stellar.sdk.xdr.ManageOfferOp;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.List;

public class ApiHelper {
    private final String BANK = "GDIRMRLHHERAEDFTIF6OR4267YANWEP2GTJRKUATUN5ZVPE2L7BA64DH";
    private final String FUND = "GBKEQXVR6UNOZA6G4R3FZW7YPXRAFROMT6IOGMLJGIOMC23YPUJVCX33";

    private final String BANK_DOMAIN = "http://auction7097.cloudapp.net:8000";
    private final String FUND_DOMAIN = "http://auction7097.cloudapp.net:8001";

    private Server bankServer;
    private Server fundServer;

    private StorageHelper storage;

    //region Accounts
    private User user1;
    private User user2;
    //endregion Accounts

    //region Singleton
    private static ApiHelper instance;

    private ApiHelper() {
        user1 = new User();
        user1.setName("Pavel Kravchenko");
        user1.setEmail("pavel.kravchenko@gmail.com");
        user1.setPicture(R.mipmap.ic_launcher_round);
        user1.setBankKeyPair(KeyPair.fromSecretSeed("SDP6RAG6IS7YBVMHVJN3UQMBAVI5UR4IZTHZIAZ2VVJ3VNTSM2UY7ENC"));
        user1.setFundKeyPair(KeyPair.fromSecretSeed("SBYZFNHB4ZMX264XUSEG4YKSOMLJFZFAR4ZO4XRGHU4OCU4PQVTDQXKC"));

        user2 = new User();
        user2.setName("John Doe");
        user2.setEmail("john.doe@gmail.com");
        user2.setPicture(R.mipmap.ic_launcher_round);
        user2.setBankKeyPair(KeyPair.fromSecretSeed("SDKO4WKZJWSAQHPGVTAGT5O7EQYO7FSVKOXU6ITN67UP23ABBO364RXZ"));
        user2.setFundKeyPair(KeyPair.fromSecretSeed("SCXL3FJCMTPIJJPJOWNJDLRCV26UHMZTU7FZSTBWTDUZAHKVMJT7UJOW"));

        bankServer = new Server(BANK_DOMAIN);
        fundServer = new Server(FUND_DOMAIN);

        storage = StorageHelper.getInstance();
    }

    public static ApiHelper getInstance() {
        return instance == null ? instance = new ApiHelper() : instance;
    }
    //endregion Singleton

    @Nullable
    public User login(String login, String password) {
        if (Strings.isNullEmptyOrWhitespaces(login) || Strings.isNullEmptyOrWhitespaces(password))
            return null;

        User user;

        if (login.equals("user1"))
            user = user1;
        else if (login.equals("user2"))
            user = user2;
        else
            return null;

        StorageHelper.getInstance().setUser(user);

        return user;
    }

    public void downloadBalance() {
        try {
            AccountResponse.Balance[] balances = bankServer.accounts().account(storage.getUser().getBankKeyPair()).getBalances();
            for (AccountResponse.Balance balance : balances) {
                if (balance.getAssetCode().equals("EUR"))
                    storage.setBalance(new Balance(balance.getDecimalBalance(), balance.getAssetCode()));
            }

            balances = fundServer.accounts().account(storage.getUser().getFundKeyPair()).getBalances();
            for (AccountResponse.Balance balance : balances) {
                if (balance.getAssetCode().equals("AAA"))
                    storage.setBalance(new Balance(balance.getDecimalBalance(), balance.getAssetCode()));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void downloadTransactions() {
        List<Transaction> eurTxs = new ArrayList<>();

        List<PaymentOperationResponse> payments = bankServer.payments().forAccount(StorageHelper.getInstance().getUser().getBankKeyPair());
        for (PaymentOperationResponse payment : payments) {
            eurTxs.add(fromPaymentResponse(payment, "EUR", storage.getUser().getBankKeyPair().getAccountId()));
        }

        storage.setTransactions(eurTxs, "EUR");

        List<Transaction> fundTxs = new ArrayList<>();

        payments = fundServer.payments().forAccount(StorageHelper.getInstance().getUser().getFundKeyPair());
        for (PaymentOperationResponse payment : payments) {
            fundTxs.add(fromPaymentResponse(payment, "AAA", storage.getUser().getFundKeyPair().getAccountId()));
        }

        storage.setTransactions(fundTxs, "AAA");
    }

    private Transaction fromPaymentResponse(PaymentOperationResponse paymentResponse, String asset, String accountId) {
        Transaction tx = new Transaction();

        if (paymentResponse.getFrom().getAccountId().equals(accountId)) {
            tx.setAddress(paymentResponse.getTo().getAccountId());
            tx.setAmount(new BigDecimal(paymentResponse.getAmount()).negate());
        } else {
            tx.setAddress(paymentResponse.getFrom().getAccountId());
            tx.setAmount(new BigDecimal(paymentResponse.getAmount()));
        }

        tx.setAsset(asset);

        return tx;
    }

    public void downloadOrderbook() {
        try {
            List<Order> orders = new ArrayList<>();

            OrderBookResponse response = bankServer.orderBook()
                    .buyingAsset(Asset.createNonNativeAsset("AAA", KeyPair.fromAccountId(FUND)))
                    .sellingAsset(Asset.createNonNativeAsset("EUR", KeyPair.fromAccountId(BANK)))
                    .execute();

            for (OfferData offer : response.getAsks()) {
                BigDecimal sourceAmount = new BigDecimal(offer.getAmount());
                BigDecimal price = new BigDecimal(offer.getPrice());
                BigDecimal distAmount = sourceAmount.multiply(price);
                Order order = new Order("EUR", "AAA", sourceAmount, distAmount);
                order.setEuroPrice(price);

                orders.add(order);
            }

            for (OfferData offer : response.getBids()) {
                BigDecimal distAmount = new BigDecimal(offer.getAmount());
                BigDecimal price = new BigDecimal(offer.getPrice());
                BigDecimal sourceAmount = distAmount.divide(price).round(new MathContext(2));
                Order order = new Order("AAA", "EUR", sourceAmount, distAmount);
                order.setEuroPrice(price);

                orders.add(order);
            }

            storage.setOrders(orders);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean createOrder(Order order) {
        Server server;
        KeyPair source, dist;
        if (order.getSourceAsset().equals("EUR")) {
            server = bankServer;
            source = storage.getUser().getBankKeyPair();
            dist = storage.getUser().getFundKeyPair();
        } else {
            server = fundServer;
            dist = storage.getUser().getBankKeyPair();
            source = storage.getUser().getFundKeyPair();
        }

         BigDecimal price = order.getDestAmount().divide(order.getSourceAmount()).round(new MathContext(2));

         ManageOfferOperation op = new ManageOfferOperation(
                 Asset.createNonNativeAsset(order.getSourceAsset(), KeyPair.fromAccountId(order.getSourceAsset().equals("EUR") ? BANK : FUND)),
                 Asset.createNonNativeAsset(order.getDistAsset(), KeyPair.fromAccountId(order.getDistAsset().equals("EUR") ? BANK : FUND)),
                 order.getSourceAmount().toPlainString(),
                 Price.fromString(price.toPlainString()),
                 0
         );

        long sequencyNumber;
        try {
            sequencyNumber = server.accounts().account(source).getSequenceNumber();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        org.stellar.sdk.Transaction tx = new org.stellar.sdk.Transaction.Builder(new Account(source, sequencyNumber))
                .addOperation(op)
                .addMemo(new MemoHash(dist.getPublicKey()))
                .build();

        tx.sign(source);

        try {
            SubmitTransactionResponse response = server.submitTransaction(tx);

            return response.isSuccess();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return false;
        }
    }
}

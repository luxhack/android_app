package com.distributedlab.hackathonluxembourg.logic;

import android.os.AsyncTask;
import android.support.annotation.Nullable;

import com.distributedlab.hackathonluxembourg.models.Order;

/**
 * Created by yaroslav on 5/9/17.
 */

public class CreateOrderTask extends AsyncTask<Order,Void,Boolean> {
    private Order order;
    private OnOrderCreated onOrderCreated;

    public CreateOrderTask(OnOrderCreated callback) {
        this.onOrderCreated = callback;
    }

    @Override
    protected Boolean doInBackground(Order... params) {
        order = params[0];
        return ApiHelper.getInstance().createOrder(order);
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);

        onOrderCreated.run(aBoolean, order);
    }

    public interface OnOrderCreated {
        void run(Boolean isSuccess, Order order);
    }
}

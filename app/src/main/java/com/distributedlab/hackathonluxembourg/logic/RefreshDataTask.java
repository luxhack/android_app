package com.distributedlab.hackathonluxembourg.logic;

import android.os.AsyncTask;

/**
 * Created by yaroslav on 5/9/17.
 */

public class RefreshDataTask extends AsyncTask<Void,Void,Void> {
    private Runnable callback;

    public RefreshDataTask(Runnable callback) {
        this.callback = callback;
    }

    @Override
    protected Void doInBackground(Void... params) {
        ApiHelper api = ApiHelper.getInstance();

        try {
            api.downloadBalance();
            publishProgress();
            api.downloadTransactions();
            publishProgress();
            api.downloadOrderbook();
            publishProgress();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);

        callback.run();
    }
}

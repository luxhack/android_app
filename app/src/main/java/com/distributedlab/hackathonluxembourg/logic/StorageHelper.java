package com.distributedlab.hackathonluxembourg.logic;

import android.content.Context;
import android.content.SharedPreferences;

import com.distributedlab.hackathonluxembourg.App;
import com.distributedlab.hackathonluxembourg.models.Balance;
import com.distributedlab.hackathonluxembourg.models.Order;
import com.distributedlab.hackathonluxembourg.models.Transaction;
import com.distributedlab.hackathonluxembourg.models.User;
import com.distributedlab.hackathonluxembourg.util.SharedPrefUtils;
import com.google.gson.reflect.TypeToken;

import org.stellar.sdk.KeyPair;

import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by yaroslav on 5/8/17.
 */

public class StorageHelper {
    //region Keys
    private final String KEY_ORDERS = "orders";
    private final String KEY_BALANCE = "balance";
    private final String KEY_TXS = "txs";
    //endregion Keys

    private SharedPreferences prefs;

    private User user;

    //region Singleton
    private static StorageHelper instance;

    private StorageHelper() {
        prefs = App.getContext().getSharedPreferences(
                "prefs", Context.MODE_PRIVATE);
    }

    public static StorageHelper getInstance() {
        return instance == null ? instance = new StorageHelper() : instance;
    }
    //endregion Singleton

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public List<Order> getOrders() {
        return get(KEY_ORDERS, new TypeToken<List<Order>>(){}.getType(), new ArrayList<Order>());
    }

    public void setOrders(List<Order> orders) {
        put(KEY_ORDERS, orders);
    }

    public Balance getBalance(String asset) {
        return getFromUserPrefs(assetKey(KEY_BALANCE, asset), Balance.class, new Balance(BigDecimal.ZERO, asset));
    }

    public void setBalance(Balance balance) {
        putToUserPrefs(assetKey(KEY_BALANCE, balance.getAsset()), balance);
    }

    public List<Transaction> getTransactions(String asset) {
        return getFromUserPrefs(assetKey(KEY_TXS, asset),
                new TypeToken<List<Transaction>>(){}.getType(), new ArrayList<Transaction>());
    }

    public void setTransactions(List<Transaction> transactions, String asset) {
        putToUserPrefs(assetKey(KEY_TXS, asset), transactions);
    }

    //region Helpers
    private String assetKey(String key, String asset) {
        return key + asset;
    }

    private String buildUserBasedKey(String key) {
        return key + user.getBankKeyPair().getAccountId();
    }

    private <T> void putToUserPrefs(String key, T value) {
        put(buildUserBasedKey(key), value);
    }

    private <T> T getFromUserPrefs(String key, Class<T> type, T defValue) {
        return get(buildUserBasedKey(key), type, defValue);
    }

    private <T> T getFromUserPrefs(String key, Type type, T defValue) {
        return get(buildUserBasedKey(key), type, defValue);
    }

    private synchronized <T> void put(String key, T value) {
        SharedPrefUtils.put(prefs, key, value);
    }

    private synchronized <T> T get(String key, Class<T> type, T defValue) {
        return SharedPrefUtils.get(prefs, key, type, defValue);
    }

    private synchronized <T> T get(String key, Type type, T defValue) {
        return SharedPrefUtils.get(prefs, key, type, defValue);
    }
    //endregion Helpers
}

package com.distributedlab.hackathonluxembourg.models;

import java.math.BigDecimal;

/**
 * Created by yaroslav on 5/8/17.
 */

public class Balance {
    private String asset;
    private BigDecimal amount;

    public Balance(BigDecimal amount, String asset) {
        this.asset = asset;
        this.amount = amount;
    }

    public String getAsset() {
        return asset;
    }

    public void setAsset(String asset) {
        this.asset = asset;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}

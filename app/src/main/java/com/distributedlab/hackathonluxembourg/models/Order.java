package com.distributedlab.hackathonluxembourg.models;

import java.math.BigDecimal;

public class Order {
    private String sourceAsset;
    private String distAsset;
    private BigDecimal sourceAmount;
    private BigDecimal destAmount;
    private BigDecimal euroPrice;

    public Order() {
    }

    public Order(String sourceAsset, String distAsset, BigDecimal sourceAmount, BigDecimal destAmount) {
        this.sourceAsset = sourceAsset;
        this.distAsset = distAsset;
        this.sourceAmount = sourceAmount;
        this.destAmount = destAmount;
    }

    public String getSourceAsset() {
        return sourceAsset;
    }

    public void setSourceAsset(String sourceAsset) {
        this.sourceAsset = sourceAsset;
    }

    public String getDistAsset() {
        return distAsset;
    }

    public void setDistAsset(String distAsset) {
        this.distAsset = distAsset;
    }

    public BigDecimal getSourceAmount() {
        return sourceAmount;
    }

    public void setSourceAmount(BigDecimal sourceAmount) {
        this.sourceAmount = sourceAmount;
    }

    public BigDecimal getDestAmount() {
        return destAmount;
    }

    public void setDestAmount(BigDecimal destAmount) {
        this.destAmount = destAmount;
    }

    public BigDecimal getEuroPrice() {
        return euroPrice;
    }

    public void setEuroPrice(BigDecimal euroPrice) {
        this.euroPrice = euroPrice;
    }

    public Order reverse() {
        return new Order(distAsset, sourceAsset, destAmount, sourceAmount);
    }
}

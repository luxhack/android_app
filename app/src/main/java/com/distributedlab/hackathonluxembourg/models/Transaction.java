package com.distributedlab.hackathonluxembourg.models;

import java.math.BigDecimal;

/**
 * Created by yaroslav on 5/8/17.
 */

public class Transaction {
    private String address;
    private BigDecimal amount;
    private String asset;

    public Transaction() {
    }

    public Transaction(String address, BigDecimal amount, String asset) {
        this.address = address;
        this.amount = amount;
        this.asset = asset;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getAsset() {
        return asset;
    }

    public void setAsset(String asset) {
        this.asset = asset;
    }

    public boolean isSent() {
        return amount.compareTo(BigDecimal.ZERO) ==  -1;
    }
}

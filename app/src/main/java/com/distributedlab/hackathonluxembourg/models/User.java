package com.distributedlab.hackathonluxembourg.models;

import android.support.annotation.DrawableRes;

import org.stellar.sdk.KeyPair;

/**
 * Created by yaroslav on 5/9/17.
 */

public class User {
    private String name;
    private String email;
    private @DrawableRes int picture;
    private KeyPair bankKeyPair;
    private KeyPair fundKeyPair;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getPicture() {
        return picture;
    }

    public void setPicture(int picture) {
        this.picture = picture;
    }

    public KeyPair getBankKeyPair() {
        return bankKeyPair;
    }

    public void setBankKeyPair(KeyPair bankKeyPair) {
        this.bankKeyPair = bankKeyPair;
    }

    public KeyPair getFundKeyPair() {
        return fundKeyPair;
    }

    public void setFundKeyPair(KeyPair fundKeyPair) {
        this.fundKeyPair = fundKeyPair;
    }
}

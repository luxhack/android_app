package com.distributedlab.hackathonluxembourg.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.distributedlab.hackathonluxembourg.R;
import com.distributedlab.hackathonluxembourg.models.Transaction;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yaroslav on 5/8/17.
 */

public class HistoryAdapter extends RecyclerView.Adapter<HistoryViewHolder> {
    private List<Transaction> txs;

    @Override
    public HistoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_holder_history, parent, false);
        return new HistoryViewHolder(v);
    }

    @Override
    public void onBindViewHolder(HistoryViewHolder holder, int position) {
        holder.bind(txs.get(position));
    }

    @Override
    public int getItemCount() {
        return txs.size();
    }

    public HistoryAdapter() {
        txs = new ArrayList<>();
    }

    public void setData(List<Transaction> txs) {
        this.txs = new ArrayList<>(txs);
        notifyDataSetChanged();
    }

    public void addTx(Transaction tx) {
        txs.add(0, tx);
        notifyItemInserted(0);
    }
}

package com.distributedlab.hackathonluxembourg.ui.adapters;

import android.content.Context;
import android.content.IntentFilter;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.distributedlab.hackathonluxembourg.App;
import com.distributedlab.hackathonluxembourg.R;
import com.distributedlab.hackathonluxembourg.models.Transaction;
import com.distributedlab.hackathonluxembourg.util.AmountFormatter;

/**
 * Created by yaroslav on 5/8/17.
 */

public class HistoryViewHolder extends RecyclerView.ViewHolder {
    public View rootView;
    private ImageView directionImage;
    private TextView fromTo;
    private TextView address;
    private TextView amount;

    public HistoryViewHolder(View v) {
        super(v);

        rootView = v;

        directionImage = (ImageView) v.findViewById(R.id.image);
        fromTo = (TextView) v.findViewById(R.id.from_to);
        address = (TextView) v.findViewById(R.id.address);
        amount = (TextView) v.findViewById(R.id.amount);
    }

    public void bind(Transaction tx) {
        if (tx.isSent()) {
            fromTo.setText(getContext().getString(R.string.to));
            amount.setTextColor(getContext().getResources().getColor(R.color.red_500));
            amount.setText("- " + AmountFormatter.formatWithCurrencySign(tx.getAmount().negate(), tx.getAsset()));
            directionImage.setImageDrawable(getContext().getResources().getDrawable(R.drawable.tx_out));
        } else {
            fromTo.setText(getContext().getString(R.string.from));
            amount.setTextColor(getContext().getResources().getColor(R.color.green_500));
            amount.setText("+ " + AmountFormatter.formatWithCurrencySign(tx.getAmount(), tx.getAsset()));
            directionImage.setImageDrawable(getContext().getResources().getDrawable(R.drawable.tx_in));
        }

        address.setText(tx.getAddress());
    }

    //region Helpers
    private Context getContext() {
        return App.getContext();
    }
    //endregion Helpers
}

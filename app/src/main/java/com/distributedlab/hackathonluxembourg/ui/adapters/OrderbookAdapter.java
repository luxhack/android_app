package com.distributedlab.hackathonluxembourg.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.distributedlab.hackathonluxembourg.R;
import com.distributedlab.hackathonluxembourg.models.Order;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yaroslav on 5/8/17.
 */

public class OrderbookAdapter extends RecyclerView.Adapter<OrderbookVH> {
    private List<Order> orders;
    private OnOrderbookClick onClick;

    @Override
    public OrderbookVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_holder_orderbook, parent, false);
        return new OrderbookVH(v, onClick);
    }

    @Override
    public void onBindViewHolder(OrderbookVH holder, int position) {
        holder.bind(orders.get(position));
    }

    @Override
    public int getItemCount() {
        return orders.size();
    }

    public OrderbookAdapter(List<Order> orders, OnOrderbookClick onClick) {
        this.orders = new ArrayList<>(orders);
        this.onClick = onClick;
        notifyDataSetChanged();
    }

    public interface OnOrderbookClick {
        void onClick(Order order);
    }
}

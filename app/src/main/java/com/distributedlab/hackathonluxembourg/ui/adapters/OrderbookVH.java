package com.distributedlab.hackathonluxembourg.ui.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.distributedlab.hackathonluxembourg.App;
import com.distributedlab.hackathonluxembourg.R;
import com.distributedlab.hackathonluxembourg.models.Order;
import com.distributedlab.hackathonluxembourg.models.Transaction;
import com.distributedlab.hackathonluxembourg.util.AmountFormatter;
import com.distributedlab.hackathonluxembourg.util.Strings;

/**
 * Created by yaroslav on 5/8/17.
 */

public class OrderbookVH extends RecyclerView.ViewHolder {
    private Order order;

    private Button action;
    private TextView source;
    private TextView destination;

    public OrderbookVH(View v, final OrderbookAdapter.OnOrderbookClick onClick) {
        super(v);

        action = (Button) v.findViewById(R.id.action);
        action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClick.onClick(order);
            }
        });
        source = (TextView) v.findViewById(R.id.source);
        destination = (TextView) v.findViewById(R.id.dest);
    }

    public void bind(Order order) {
        action.setText(getContext().getString(R.string.match));
        source.setText(AmountFormatter.formatWithCurrencySign(order.getSourceAmount(), order.getSourceAsset()));
        destination.setText(AmountFormatter.formatWithCurrencySign(order.getDestAmount(), order.getDistAsset()));
    }

    //region Helpers
    private Context getContext() {
        return App.getContext();
    }
    //endregion Helpers
}

package com.distributedlab.hackathonluxembourg.ui.adapters;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.Context;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by yaroslav on 5/8/17.
 */

public class TxHistoryItemAnimator extends DefaultItemAnimator {
    private Context context;

    public TxHistoryItemAnimator(Context context) {
        this.context = context;
    }

    @Override
    public boolean animateAdd(final RecyclerView.ViewHolder holder) {
        final View viewToAnimate = ((HistoryViewHolder) holder).rootView;

        final int animationTime = context.getResources().getInteger(
                android.R.integer.config_mediumAnimTime);
        final int shortAnimationTime = context.getResources().getInteger(
                android.R.integer.config_shortAnimTime);

        viewToAnimate.setTranslationX(viewToAnimate.getWidth() * 1f);
        viewToAnimate.setAlpha(0f);

        // Animate alpha and height of the placeholder.
        holder.itemView.setAlpha(0f);
        holder.itemView.animate()
                .alpha(1f)
                .setInterpolator(new FastOutSlowInInterpolator())
                .setDuration(animationTime)
                .start();

        ValueAnimator heightAnimator = ValueAnimator.ofInt(0, holder.itemView.getHeight());
        heightAnimator.setDuration(shortAnimationTime).setInterpolator(new FastOutSlowInInterpolator());
        heightAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                int value = (int) animation.getAnimatedValue();
                holder.itemView.getLayoutParams().height = value;
                holder.itemView.requestLayout();
            }
        });

        // Then animate tx slide from right to left.
        heightAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                viewToAnimate.animate()
                        .translationX(0f)
                        .alpha(1f)
                        .setInterpolator(new FastOutSlowInInterpolator())
                        .setDuration(animationTime)
                        .start();
            }
        });

        heightAnimator.start();

        return true;
    }

    @Override
    public boolean animateChange(RecyclerView.ViewHolder oldHolder, RecyclerView.ViewHolder newHolder, int fromX, int fromY, int toX, int toY) {
        return true;
    }

    @Override
    public boolean canReuseUpdatedViewHolder(RecyclerView.ViewHolder viewHolder) {
        return true;
    }
}

package com.distributedlab.hackathonluxembourg.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;

/**
 * Created by yaroslav on 5/8/17.
 */

public class AmountFormatter {
    private static final int FIAT_SCALE = 2;

    /**
     * Formats amount with adding currency or denomination sign for BTC
     * @return formatted string
     */
    public static String formatWithCurrencySign(BigDecimal amount, String currency) {;
        return String.format("%s %s", format(amount), currency);
    }

    public static String format(BigDecimal amount){
        return format(amount, FIAT_SCALE, 0);
    }

    public static String format(BigDecimal amount, int maxZerosCount, int minZerosCount){
        return buildDecimalFormatter(maxZerosCount, minZerosCount).format(amount);
    }

    //region Helpers
    private static DecimalFormatSymbols buildFormatSymbols() {
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator(',');
        symbols.setGroupingSeparator('.');

        return symbols;
    }

    private static NumberFormat buildDecimalFormatter(int maxZerosCount, int minZerosCount) {
        DecimalFormat df = new DecimalFormat();
        df.setRoundingMode(RoundingMode.HALF_UP);
        df.setDecimalFormatSymbols(buildFormatSymbols());
        df.setMaximumFractionDigits(maxZerosCount);
        df.setMinimumFractionDigits(minZerosCount);
        df.setGroupingUsed(true);

        return df;
    }
    //endregion Helpers
}

package com.distributedlab.hackathonluxembourg.util;

import android.content.SharedPreferences;

import com.google.common.base.*;
import com.google.gson.Gson;

import java.lang.reflect.Type;

/**
 * Created by yaroslav on 5/8/17.
 */

public class SharedPrefUtils {
    public static <T> void put(SharedPreferences storage, String key, T value) {
        Gson gson = new Gson();
        String json = gson.toJson(value);

        SharedPreferences.Editor editor = storage.edit();
        editor.putString(key, json);
        editor.apply();
    }

    public static <T> T get(SharedPreferences strorage, String key, Class<T> type, T defaultValue) {
        return get(strorage, key, (Type)type, defaultValue);
    }

    public static <T> T get(SharedPreferences strorage, String key, Type type, T defaultValue) {
        String json = strorage.getString(key, null);

        if (com.google.common.base.Strings.isNullOrEmpty(json))
            return defaultValue;

        Gson gson = new Gson();
        return gson.fromJson(json, type);
    }
}

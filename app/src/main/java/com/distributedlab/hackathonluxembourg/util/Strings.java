package com.distributedlab.hackathonluxembourg.util;

/**
 * Created by yaroslav on 5/8/17.
 */

public class Strings {
    public static boolean isNullEmptyOrWhitespaces(String string) {
        return string == null || string.trim().isEmpty();
    }
}
